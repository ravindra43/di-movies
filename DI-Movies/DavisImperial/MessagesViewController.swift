//
//  MessagesViewController.swift
//  DavisImperial
//
//  Created by Ravindra Singh on 08/01/18.
//  Copyright © 2018 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD
class MessagesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noItemsLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    @IBOutlet weak var messageInput: UITextView!
    @IBOutlet weak var inputHeight: NSLayoutConstraint!
    var showBack = false
    var refreshControl = UIRefreshControl.init()
    var progressHud : MBProgressHUD?
    var socket = SocketIOManager()
    var needsToRefresh = false
    fileprivate var messageDataSource : MessagesDataSource!
    
    static func getInstance() -> MessagesViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: "messagesViewController") as! MessagesViewController
    }
    
    fileprivate var dataSource : MessagesDataSource!

    override func viewDidAppear(_ animated: Bool) {
        if isMessageSent {
            onRefresh()
            isMessageSent = false;
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Message"
        messageTableView.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        messageTableView.separatorStyle =  .none
        messageInput.delegate = self
        socket.delegate = self as Socketdelegate
        let sendImage = #imageLiteral(resourceName: "send").withRenderingMode(.alwaysTemplate)
        sendButton.setImage(sendImage, for: .normal)
        sendButton.imageView?.contentMode = .scaleAspectFit
        sendButton.tintColor = UIColor(named: Color.invertColor)
        let tap = UITapGestureRecognizer(target: self, action: #selector(onCancel))
        tap.cancelsTouchesInView = false
        self.messageTableView.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UITextView.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UITextView.keyboardWillHideNotification, object: nil)
        
        self.messageDataSource = MessagesDataSource.init(success: {
             [weak self] () -> Void in self?.itemsLoaded()
        }) {
             [weak self] () -> Void in self?.itemLoadFailed()
        }
        showLoading()
        self.messageDataSource.reloadData()
        
        if showBack {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(onCancel))
        }
    }
    
    @objc func onCancel(){
        if showBack {
            NotificationCenter.default.post(name: UserManager.userDidLogin, object: nil)
            return
        }
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigation(hide: false)
    }
    @objc private func keyboardWillShow (_ sender : Notification){
        if let dict = sender.userInfo as NSDictionary? as! [String:Any]? {
            let keyboardFrameValue : NSValue = dict[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let keyboardFrame = keyboardFrameValue.cgRectValue;
            let height        = keyboardFrame.height
            self.animateKeyboard(value: height)
        }
    }
    
    @objc private func keyboardWillHide (_ sender : Notification){
        if (sender.userInfo as NSDictionary? as! [String:Any]?) != nil {
            animateKeyboard(value: 10)
        }
    }
    
    func animateKeyboard(value: CGFloat) {
        UIView.animate(withDuration: 0.3) {
            self.bottom.constant = value + 10
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func onComposeClick()  {
        let composeVC = Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTNewMessageVCID);
        let navCtrl = UINavigationController.init(rootViewController: composeVC)
        navCtrl.modalPresentationStyle = .fullScreen
        self.present(navCtrl, animated: true, completion: nil)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func onBackBarButtonTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func onMessagesStatusChangedNotification() {
        self.needsToRefresh = true
    }
    
    @objc func onRefresh(){
        self.noItemsLabel.isHidden = true
        self.dataSource.reloadData()
    }
    
    private func itemsLoaded()  {
        self.hideLoading()
        self.refreshControl.endRefreshing()
        self.messageTableView.reloadData()
        self.needsToRefresh = false
//        self.noItemsLabel.text = "No items found."
//        self.noItemsLabel.isHidden = self.dataSource.items.count != 0
        if messageDataSource.items.count > 0 && messageDataSource.page == 1{
            self.messageTableView.scrollToRow(at: IndexPath(row: messageDataSource.items.count - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
    private func itemLoadFailed()  {
        self.hideLoading()
        self.refreshControl.endRefreshing()
        self.messageDataSource.items = []
        self.messageTableView.reloadData()
//        self.noItemsLabel.text = "Something went wrong. Couldn't load items."
//        self.noItemsLabel.isHidden = false;
    }
    
    fileprivate func showLoading(){
        self.progressHud = MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    fileprivate func hideLoading(){
        self.progressHud?.hide(animated: true)
    }
    @IBAction func onPressSend(_ sender: Any) {
        let message = messageInput.text
        showLoading()
        if message != "" {
            APIRequest.sendMessage(message: message!) { (request, response, error) in
                self.hideLoading()
                let responseVO = ResponseVO(response: response, error: error)
                if responseVO.isSuccess{
                    
                    if let result = responseVO.response as? [String : Any] {
                        if let messageObj = result["msgObject"] as? [String : Any]{
                            let messageVO: MessagesVO? = MessagesVO(json: messageObj as [String : Any])!
                            if messageVO != nil {
                                self.messageDataSource.items.append(messageVO!)
                                let indexPath = IndexPath(row: self.messageDataSource.items.count - 1, section: 0)
                                self.messageTableView.insertRows(at: [indexPath], with: .bottom)
                                self.messageTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                                self.messageInput.text = ""
                                self.inputHeight.constant = 36
                            }
                        }
                    }
                }
            }
        }
    }
}

extension MessagesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageDataSource.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "msgCell", for: indexPath) as! MessagesCell
        cell.setMessage(msg: messageDataSource.items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension MessagesViewController: UITextViewDelegate, Socketdelegate{
    func textViewDidChange(_ textView: UITextView) {
        let height = (self.messageInput.contentSize.height)
        if height > 120 {
            inputHeight.constant = 120
        }else if height < 36{
            inputHeight.constant = 36
        }else{
            inputHeight.constant = height
        }
    }
    
    func didReceivedMessage(messageObj: [String : Any]) {
        let messageVO = MessagesVO(json: messageObj)
        messageDataSource.items.append(messageVO!)
        let indexPath = IndexPath(row: messageDataSource.items.count - 1, section: 0)
        messageTableView.insertRows(at: [indexPath], with: .bottom)
        messageTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
}
