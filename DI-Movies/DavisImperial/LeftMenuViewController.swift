//
//  LeftMenuViewController.swift
//  DavisImperial
//
//  Created by Surendra on 12/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import MessageUI

let leftMenuRowIndexBookings = 0
let leftMenuRowIndexNotifications = 1
let leftMenuRowIndexMessages = 2
let leftMenuRowIndexChangePassword = 3
let leftMenuRowIndexLogout = 5

let leftMenuCell = "leftMenuCell";

class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GuidedTourViewControllerDelegate {
    
    @IBOutlet weak var tableView : UITableView!;
    var menus : Array<String>!;
    var selectedRow : Int!;

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        self.menus = Array.init(arrayLiteral: "My Bookings", "Messages" ,"Change Password", "Logout");
        self.selectedRow = -1;
    }

    func userSignedIn(_ sender : NotificationCenter) {
        self.menus[leftMenuRowIndexLogout] = "Logout";
        self.tableView.reloadData();
    }

    func userSignedOut() {
        self.menus[leftMenuRowIndexLogout] = "Login";
        self.tableView.reloadData();
    }
    
    @objc func onProfilePicTap() {
        self.slideMenuController()?.closeLeft();
        let navigationController = self.slideMenuController()?.mainViewController as! UINavigationController;
        let vc = UserProfileViewController.getInstance()
        vc.user = UserManager.shared.currentUser
        navigationController.pushViewController(vc, animated: true)
    }
    
    @objc func onProfilePicLongTap() {
        let email = MFMailComposeViewController.init()
        email.setMessageBody(PushNotificationManager.shared.apnsToken!, isHTML: false)
        self.slideMenuController()?.present(email, animated: true, completion: nil)
    }

    
//MARK:- TableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        self.slideMenuController()?.closeLeft();
        if (self.selectedRow == indexPath.row) {
            return;
        }
        
        let navigationController = self.slideMenuController()?.mainViewController as! UINavigationController;
        
        switch indexPath.row {
            
        case 0:
            let bookingsVC = BookingsViewController.getInstance();
            navigationController.pushViewController(bookingsVC, animated: true)
            break;

        case 1:
            let msgVC = MessagesViewController.getInstance();
            navigationController.pushViewController(msgVC, animated: true)
            break;
        case 2:
            let msgVC = ChangePasswordViewController.getInstance();
            navigationController.pushViewController(msgVC, animated: true)
            break;
        case 3:
            UserManager.logout();
            
        default: break
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menus.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: leftMenuCell, for: indexPath) as! LeftMenuCell;
        cell.menuTitle.text = self.menus[indexPath.row];
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib.init(nibName: "MenuHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as? ProfileHeaderView
        let label = view?.nameLabel
        label?.text = UserManager.shared.currentUser?.firstName
        view?.profilePic.isUserInteractionEnabled = true
        view?.profilePic.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onProfilePicTap)))
        if modeDebug {
            let longTap = UILongPressGestureRecognizer.init(target: self, action: #selector(onProfilePicLongTap))
            longTap.minimumPressDuration = 5
            view?.profilePic.addGestureRecognizer(longTap)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 200;
    }
    
    func onGuidedTourVisitCompleted(guidedTour: GuidedTourViewController, forSignin: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
}

extension SlideMenuController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
