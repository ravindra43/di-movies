//
//  UFTextField.swift
//  DavisImperial
//
//  Created by Surendra on 6/28/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class UFTextField: UITextField {
    
    @IBInspectable var leftImage : UIImage?
    override func awakeFromNib() {
        super.awakeFromNib()
        if self.leftImage != nil {
            let imageView = UIImageView()
            imageView.image = self.leftImage
            imageView.contentMode = .center
            self.leftView = imageView
            self.leftViewMode = .always
        }
        
        self.textAlignment = .left
        
        self.layer.cornerRadius = self.bounds.size.height/2.0
        self.layer.borderColor = UIColor(named: Color.buttonBackgroundColor)?.cgColor
        self.layer.borderWidth = 1.0 / UIScreen.main.scale
        self.textColor = UIColor(named: Color.textLabelColor)
    }
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect.init(x: 0, y: 0, width: 40, height: 40)
    }
}
