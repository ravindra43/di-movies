//
//  PushNotificationManager.swift
//  DavisImperial
//
//  Created by Surendra on 7/6/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class PushNotificationManager: NSObject {
    var apnsToken : String?
    
    static let shared = PushNotificationManager();
    
    static func registerForPushNotifications() {
        if (PushNotificationManager.shared.apnsToken != nil) {
            PushNotificationManager.shared.registerToken()
            return
        }
        let settings: UIUserNotificationSettings = UIUserNotificationSettings.init(types: [.alert, .badge, .sound], categories: nil)
        let application = UIApplication.shared
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
    }
    
    func deviceApnsTokenReceived(apnsToken: String) {
        self.apnsToken = apnsToken
        self.registerToken()
    }
    
    func registerToken() {
        _ = APIRequest.registerAPNSToken(apnsToken: self.apnsToken)
    }
    
    func onPushNotificationReceive(userInfo:[AnyHashable : Any], isAppActive: Bool) {
        print("USER_INFO \(userInfo)")
        var title : String = ""
        var message : String = ""
        if let aps = userInfo["aps"] as? [AnyHashable : Any] {
            if let alert = aps["alert"] as? NSDictionary {
                if let _message = alert["body"] as? String {
                    message = _message
                }
                if let _title = alert["title"] as? String {
                    title = _title
                }
                
            } else if let alert = aps["alert"] as? String {
                message = alert
            }
            
        }
        
        
        var bookingId : UInt32?
        
        if let appData = userInfo["addData"] as? [AnyHashable : Any] {
            if let _bId = appData["b_id"] as? UInt32 {
                bookingId = _bId
            }
        }
        
        if isAppActive && title.count > 0 || message.count > 0 {
            let alert = UIAlertController.init(title: title, message:message, preferredStyle: UIAlertController.Style.alert);
            if bookingId != nil {
                alert.addAction(UIAlertAction.init(title: "View", style: .default, handler: { (action) in
                    //self.showBookingDetails(bookingId: bookingId!)
                }))
                alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                
            } else {
                alert.addAction(UIAlertAction.init(title: "Dismiss", style: UIAlertAction.Style.cancel, handler: nil))
            }
            
            alert.showAlert();
        } 
    }
    
}

// UIApplicationDelegate wrapper
extension PushNotificationManager {
    
    
    static func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("APNS TOKEN:  " + token)
        PushNotificationManager.shared.deviceApnsTokenReceived(apnsToken: token)
    }
    
    static func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
        if !Globals.isProd() {
            //Globals.showGenericErrorAlert(title: "APNS registraition failed", message: error.localizedDescription)
        }
    }
    
    static func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("NOTIFICATION", userInfo)
        PushNotificationManager.shared.onPushNotificationReceive(userInfo: userInfo, isAppActive: application.applicationState == .active)
    }
    
    static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) {
        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            PushNotificationManager.shared.onPushNotificationReceive(userInfo:userInfo , isAppActive: application.applicationState == .active)
        }
    }
}
