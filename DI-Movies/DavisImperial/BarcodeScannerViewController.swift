//
//  BarcodeScannerViewController.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 13/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import UIKit
import AVFoundation
import MBProgressHUD
class BarcodeScannerViewController: UIViewController {

    @IBOutlet weak var qrtableView: UITableView!
    
    var barArray = [String]()
    var timer = Timer()
    var canScanAgain = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inventory Numbers"
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "add"), style: .plain, target: self, action: #selector(onPressAddBarcode))
    }
    func isQRAlready(string: String) -> Bool {
        for i in barArray {
            if i == string {
                return true
            }
        }
        return false
    }
    
    static func getInstance() -> BarcodeScannerViewController{
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTQRCodeVCID) as! BarcodeScannerViewController
    }
    
    @objc func onPressAddBarcode() {
        if ServiceDS.shared.numberOfBags != nil {
            if barArray.count < ServiceDS.shared.numberOfBags! {
                Globals.showGenericAlert(title: "Enter Inventory numbers") { (code) in
                    if code != ""{
                            self.barArray.append(code)
                            self.qrtableView.reloadData()
                    }
                }
            }
        }
    }
    
    @IBAction func onPressNext(_ sender: Any) {
        ServiceDS.shared.barCodes = barArray
        let vc = UserCommentsViewController.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onPressSkip(_ sender: Any) {
        ServiceDS.shared.barCodes = []
        let vc = UserCommentsViewController.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension BarcodeScannerViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return barArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor(named: Color.messageRecieverColor)
        cell.textLabel?.text = "\(indexPath.row + 1). " + barArray[indexPath.row]
        cell.textLabel?.numberOfLines = 2
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            barArray.remove(at: indexPath.row)
            qrtableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        qrtableView.deselectRow(at: indexPath, animated: true)
    }
}
