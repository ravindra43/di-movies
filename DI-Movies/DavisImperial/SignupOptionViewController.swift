//
//  SignupOptionViewController.swift
//  DavisImperial
//
//  Created by Surendra on 9/20/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class SignupOptionViewController: UIViewController {

    @IBOutlet weak var yesButton : UIButton!;
    @IBOutlet weak var noButton : UIButton!;
    
    private var barTintColor : UIColor?

    static func getInstance() -> SignupOptionViewController {
        return Globals.welcomeStoryboard().instantiateViewController(withIdentifier: "signupOptionViewController") as! SignupOptionViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0)], for: .normal)
        self.yesButton.layer.cornerRadius = self.yesButton.bounds.size.height/2.0;
        self.yesButton.layer.borderWidth = 1.0 / UIScreen.main.scale;
        self.yesButton.layer.borderColor = UIColor.white.cgColor
        
        self.noButton.layer.cornerRadius = self.noButton.bounds.size.height/2.0;
        self.noButton.layer.borderWidth = 1.0 / UIScreen.main.scale;
        self.noButton.layer.borderColor = UIColor.white.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.barTintColor = self.navigationController?.navigationBar.tintColor
//        self.navigationController?.navigationBar.tintColor = .white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.navigationBar.tintColor = self.barTintColor
    }
    
    func onBackTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onYesButtonTap () {
        self.showSignup(false)
    }
    
    @IBAction func onNoButtonTap () {
        self.showSignup(true)
    }
    
    func showSignup(_ showCC : Bool) {
        self.navigationController?.pushViewController(SignupTableViewController.getInstance(showCC), animated: true)
    }

}
