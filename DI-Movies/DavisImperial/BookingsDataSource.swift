//
//  BookingsDataSource.swift
//  Davis Imperial
//
//  Created by Surendra on 8/11/17.
//  Copyright © 2017 Surendra. All rights reserved.
//


class BookingsDataSource: BaseDataSource {
    
    var items: [BookingVO] = [];
    
    private var apiRequest : APIRequest?
    
    override func reloadData() {
        super.reloadData()
    }
    
    override func loadItems() {
        let limit = 100000
        if self.status != .loading {
            self.status = .loading;
            weak var weakSelf = self
            self.apiRequest = APIRequest.getBookings(pageNo: Int(page), limit: limit, result: { (request, response, error) in
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    if (weakSelf?.isReloading)! {
                        weakSelf?.items = []
                        weakSelf?.isReloading = false
                    }
                    if let result = response as? [String : Any] {
                        if let resultObj = result["bookings"] as? [[String : Any]] {
                            weakSelf?.status = .idle
                            weakSelf?.allItemsLoaded = resultObj.count < limit
                            var newItems : [BookingVO] = []
                            for json in resultObj {
                                if let booking = BookingVO.init(json: json) {
                                    newItems.append(booking)
                                }
                            }
                            weakSelf?.items += newItems
                            
                            weakSelf?.success?()
                        }
                    }
                } else {
                    weakSelf?.status = .failed
                    weakSelf?.failure?()
                    if (response != nil ) {
                        if let responseObject = response as? [String: Any] {
                            if let errorString = responseObject["error"] as? String {
                                Globals.showGenericErrorAlert(title: errorString, message: "")
                                if errorString == "Unauthenticated." {
                                    UserManager.logout()
                                }
                            }
                        }
                    }
                }
            })
        }
    }
}
