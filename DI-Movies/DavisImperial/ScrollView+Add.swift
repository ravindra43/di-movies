//
//  ScrollView+Add.swift
//  DavisImperial
//
//  Created by Surendra on 7/1/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit


extension UIScrollView {
    func shouldLoadMoreItems() -> Bool {
        return self.contentOffset.y/(self.contentSize.height - self.frame.size.height) > 1
    }
    func srollReachedAttop() -> Bool {
        return self.contentOffset.y <= 0
    }
}
