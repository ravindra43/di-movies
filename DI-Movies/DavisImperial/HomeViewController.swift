//
//  HomeViewController.swift
//  DavisImperial
//
//  Created by Surendra on 8/26/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
var unreadBadgeCount: Int = 0;

class HomeViewController: BaseViewController {

    @IBOutlet weak var pickupButton : UIButton!
    @IBOutlet weak var buttonBackView : UIVisualEffectView!
    
    
    var apiRequest : APIRequest?
    
    
    static func getInstance() -> HomeViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: "homeViewController") as! HomeViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonBackView.layer.cornerRadius = 22.0;
        self.buttonBackView.clipsToBounds = true
        self.pickupButton.layer.cornerRadius = 22.0;
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "topbarMenu"), style: .plain, target: self, action: #selector(onPressMenu))
    }
  
    func moveToMessage() {
        let navigationController = self.slideMenuController()?.mainViewController as! UINavigationController;
        let msgVC = MessagesViewController.getInstance();
        navigationController.pushViewController(msgVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    override func viewWillDisappear(_ animated: Bool) {
        showNavigationBar()
    }
    
    @objc func onPressMenu(){
        self.didTapTopMenu()
    }

    @IBAction func onRequestAPickupTap() {
        let vc = PickUpSelectionViewController.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPressTalkToUs(_ sender: Any) {
        let vc = MessagesViewController.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPressSendLocation(_ sender: Any) {
        if #available(iOS 9.3, *) {
            let locationVC = LocationViewController.getInstance()
            locationVC.isSendLocation = true
            self.navigationController?.pushViewController(locationVC, animated: true)
        }
    }
}

extension HomeViewController : RequestPickupViewControllerDelegate {
    func onBookingDone(requestPickupVC: RequestPickupViewController) {
        self.navigationController?.popToViewController(self, animated: false)
        self.navigationController?.pushViewController(BookingsViewController.getInstance(), animated: true)
    }
}
