//
//  MessagesCell.swift
//  DavisImperial
//
//  Created by Ravindra Singh on 09/01/18.
//  Copyright © 2018 Encoresky. All rights reserved.
//

import UIKit

class MessagesCell: UITableViewCell {
    
    var messageText : UILabel = {
        let label = UILabel()
        label.numberOfLines = 50
        return label
    }()
    
    var dateLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.text = "02/02/2019 2:30 PM"
        label.textColor = UIColor(named: Color.darkGrayColor)
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont(descriptor: label.font.fontDescriptor, size: 10)
        return label
    }()
    
    var messageView : UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        view.layer.shadowRadius = 1
        view.layer.shadowOpacity = 0.6
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        
    }
    
    func setMessage(msg: MessagesVO) {
        removeSubViews(view: messageView)
        removeSubViews(view: contentView)

        self.messageView.addSubview(messageText)
        self.contentView.addSubview(dateLabel)
        self.contentView.addSubview(messageView)
        messageText.text = (msg.message)
        messageText.sizeToFit()
        dateLabel.text   =  Globals.formattedDateTime(date: msg.createdDate)
        self.messageView.addConstraintsWithFormat(format: "V:|-8-[v0]-8-|", views: messageText)
        self.messageView.addConstraintsWithFormat(format: "H:|-9-[v0]-9-|", views: messageText)
        self.contentView.addConstraintsWithFormat(format: "V:|-5-[v0]-20-|", views: messageView)
        self.contentView.addConstraintsWithFormat(format: "V:[v0(15)]-2-|", views: dateLabel)
        let messageWidth = UIScreen.main.bounds.width/1.2
        messageText.textColor            = UIColor(named: Color.textLabelColor)
        if msg.isSender! {
            self.messageView.backgroundColor = UIColor(named: Color.messageSenderColor)
            self.contentView.addConstraintsWithFormat(format: "H:[v0(<=\(messageWidth))]-10-|", views: messageView)
            self.contentView.addConstraintsWithFormat(format: "H:[v0]-20-|", views: dateLabel)

        }else{
            self.messageView.backgroundColor = UIColor(named: Color.messageRecieverColor)
            self.contentView.addConstraintsWithFormat(format: "H:|-10-[v0(<=\(messageWidth))]", views: messageView)
            self.contentView.addConstraintsWithFormat(format: "H:|-20-[v0]", views: dateLabel)
        }
    }
    func removeSubViews(view: UIView)  {
        for v in view.subviews {
            v.removeFromSuperview()
        }
    }
}
