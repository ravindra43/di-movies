//
//  PickUpSelectionViewController.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 09/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import UIKit

class PickUpSelectionViewController: UIViewController {

    @IBOutlet weak var asapVisualView: UIVisualEffectView!
    @IBOutlet weak var scheduleView: DIView!
    
    @IBOutlet weak var asapButton: UIButton!
    @IBOutlet weak var scheduleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.asapVisualView.layer.cornerRadius = self.asapVisualView.frame.height/2;
        self.asapVisualView.clipsToBounds = true;
        self.scheduleView.layer.cornerRadius = self.scheduleView.frame.height/2;
        self.scheduleView.layer.borderWidth = 1.0 / UIScreen.main.scale;
        self.scheduleView.layer.borderColor = UIColor.white.cgColor
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()

    }
   override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNormalBackButton(color: .white, image: UIImage())
        self.navigationController?.setNavigation(hide: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController!.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController!.navigationBar.shadowImage = nil
        self.navigationController?.setNavigation(hide: false)
    }
    static func getInstance() -> PickUpSelectionViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTPickUpSelectVCID) as! PickUpSelectionViewController
    }
    
    @IBAction func didTapASAP(_ sender: Any) {
        if #available(iOS 9.3, *) {
            ServiceDS.shared.scheduleType = .asap
            let vc  = LocationOptionViewController.getInstance()
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func didTapSchedule(_ sender: Any) {
        ServiceDS.shared.scheduleType = .schedule
        let vc = RequestPickupViewController.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
