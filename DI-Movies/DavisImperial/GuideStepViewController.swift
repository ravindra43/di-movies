//
//  GuideStepViewController.swift
//  DavisImperial
//
//  Created by Surendra on 15/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import TTTAttributedLabel

let imageKey = "pageImageKey";
let titleKey = "pageTitleKey";
let descriptionKey = "pageDescriptionKey";

class GuideStepViewController: UIViewController, TTTAttributedLabelDelegate {

    @IBOutlet weak var backgroundImageView : UIImageView!;
    @IBOutlet weak var titleLabel : UILabel!;
    @IBOutlet weak var descriptionLabel : TTTAttributedLabel!;
    
    var dictionary : Dictionary<String, String>!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.font = Font.headingFontOf(size: 21.0)
        self.descriptionLabel.font = Font.regularFontOf(size:12.0)
        self.backgroundImageView.image = UIImage.init(named: self.dictionary[imageKey]!);
        self.descriptionLabel.tintColor = UIColor.white
        self.descriptionLabel.linkAttributes = [NSAttributedString.Key.underlineStyle:NSNumber.init(integerLiteral: 1), NSAttributedString.Key.foregroundColor:UIColor.white]
        self.descriptionLabel.delegate = self
        self.titleLabel.text = self.dictionary[titleKey];
        self.descriptionLabel.text = self.dictionary[descriptionKey];
        
        let nsStr = self.descriptionLabel.text!

        let linkRange = (nsStr as AnyObject).range(of: "www.davisimperial.com")
        if linkRange.location != NSNotFound {
            self.descriptionLabel.addLink(to: URL.init(string: "http://www.davisimperial.com"), with: linkRange)
        }
        
        let phoneRange = (nsStr as AnyObject).range(of: "866.267.4560")
        if phoneRange.location != NSNotFound {
            self.descriptionLabel.addLink(to: URL.init(string: "tel://8662674560"), with: phoneRange)
        }
        
        let emailRange = (nsStr as AnyObject).range(of: "info@davisimperial.com")
        if emailRange.location != NSNotFound {
            self.descriptionLabel.addLink(to: URL.init(string: "mailto://info@davisimperial.com"), with: emailRange)
        }
        
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
}
