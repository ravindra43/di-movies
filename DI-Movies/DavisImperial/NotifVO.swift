//
//  NotifVO.swift
//  DavisImperial
//
//  Created by Surendra on 8/26/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class NotifVO: NSObject {
    var title : String!
    var message : String?
    var createdDate : Date?

    init(title: String) {
        super.init();
        self.title = title
    }
}

extension NotifVO {
    convenience init?(json: [String: Any]) {
        guard let _title = json["booking_notification"] as? String
            else {
                return nil
        }
        
        self.init(title: _title);
        
        
        if let _msg = json["booking_comment"] as? String {
            self.message = _msg
        }
        
        if let _date = json["created_at"] as? String {
            self.createdDate = Globals.dateFromString(string: _date)
        }
    }
    
    convenience init?(activtyJson: [String: Any]) {
        guard let _title = activtyJson["message"] as? String
            else {
                return nil
        }
        
        self.init(title: _title);
        
        if let _date = activtyJson["created_at"] as? String {
            self.createdDate = Globals.dateFromString(string: _date)
        }
    }
}
