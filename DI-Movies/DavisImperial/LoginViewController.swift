//
//  LoginViewController.swift
//  DavisImperial
//
//  Created by Surendra on 12/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD
import LocalAuthentication

class LoginViewController: KeyboardReadyViewController {
    
    @IBOutlet weak var usernameTextField : UITextField!; // email
    @IBOutlet weak var passwordTextField : UITextField!;
    @IBOutlet weak var formView : UIView!;

    @IBOutlet weak var signinButton : UIButton!;
    @IBOutlet weak var signupButton : UIButton!;
    var apiRequest : APIRequest?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.authenticateUserUsingTouchId()
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13, *) {
            self.navigationController?.setNavigation(hide: true)
        }else{
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    fileprivate func authenticateUserUsingTouchId() {
        let context = LAContext()
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) || context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil){
            KeychainManager.shared.evaulateTocuhIdAuthenticity(context: context, result: { (userName, password) in
                DispatchQueue.main.async {
                    self.loginUser(userName: userName, password: password)
                }
            })
        }
    }
    
    
    private func setupUI(){
        self.formView.layer.cornerRadius = 5.0;
        self.formView.clipsToBounds = true;
    }
    
    @IBAction func didTapOnSignupButton() {
        self.navigationController?.pushViewController(SignupViewController.getInstance(), animated: true)
    }
    
    @IBAction func didTapOnPorgotPasswordButton() {
        let vc = ResetPasswordViewController.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapLoginButton() {
        self.currentTextField?.resignFirstResponder()
        
        let emailOrPhone = self.usernameTextField.text?.trim()
        let password = self.passwordTextField.text?.trim()
        loginUser(userName: emailOrPhone, password: password)
        
    }
    
    func loginUser(userName: String?, password: String?) {
        var errorTitle : String? = nil
        
        if userName == nil || userName == "" {
            errorTitle = "Please enter email"
        } else if !Globals.isValidEmail(testStr: userName!) {
            errorTitle = "Invalid email"
        } else if password == nil || password == "" {
            errorTitle = "Please enter password"
        }
        
        if errorTitle != nil {
            self.showErrorAlert(title: errorTitle!, message: "")
            
        }else {
            if !APIRequest.isNetworkAvailable() {
                Globals.shared.showNoInternetConeectionAlert()
                return;
            }
            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.apiRequest = APIRequest.login(email: userName!, password: password!) { [weak self] (requets, response, error) in
                progressView.hide(animated: true)
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    if let result = responseVO.response as? [String: Any] {
                        let userObj = result["user"] as! [String : Any]
                        let token = result["token"] as! String
                        let tokenId = ""
                        let user = UserVO.init(json: userObj)
                        UserManager.shared.setSessionToken(token: token, tokenId:tokenId)
                        UserManager.shared.updateCurrentUser(user: user)
                        NotificationCenter.default.post(name: UserManager.userDidLogin, object: nil)
                        KeychainManager.shared.saveAccountDetailsToKeychain(account: userName!, password: password!)
                    }
                } else {
                    if let errorMessage = responseVO.errorMessage {
                        self?.showErrorAlert(title: errorMessage, message: "")
                    } else {
                        Globals.showGenericErrorAlert()
                    }
                }
            }
        }
    }
    
    func showErrorAlert(title: String?, message: String?) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Dismiss", style: .cancel, handler: nil))
        alert.showAlert()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
}


