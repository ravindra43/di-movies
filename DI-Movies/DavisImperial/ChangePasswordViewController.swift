//
//  ChangePasswordViewController.swift
//  DavisImperial
//
//  Created by Surendra on 7/12/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD

class ChangePasswordViewController: KeyboardReadyViewController {

    @IBOutlet weak var oldPasswordTextField : UITextField!;
    @IBOutlet weak var newPasswordTextField : UITextField!;
    @IBOutlet weak var confirmNewPasswordTextField : UITextField!;

    @IBOutlet weak var formView : UIView!;
    
    @IBOutlet weak var chandePasswordButton : UIButton!;
    
    var changePasswordRequest : APIRequest?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    private func setupUI(){
        self.formView.layer.cornerRadius = 5.0;
        self.formView.clipsToBounds = true;
        self.title = "Change Password"
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigation(hide: false)
    }
    @objc func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    static func getInstance() -> ChangePasswordViewController {
        return Globals.welcomeStoryboard().instantiateViewController(withIdentifier: kLTChangePasswordVCID) as! ChangePasswordViewController
    }
    @IBAction func didTapChangePasswordButton() {
        self.currentTextField?.resignFirstResponder()
        
        let oldPassword = self.oldPasswordTextField.text?.trim()
        let newPassword = self.newPasswordTextField.text?.trim()
        let cNewPassword = self.confirmNewPasswordTextField.text?.trim()

        var errorTitle : String? = nil
        
        if oldPassword == nil || oldPassword == "" {
            errorTitle = "Please enter old password"
            
        } else if newPassword == nil || newPassword == "" {
            errorTitle = "Please enter new password"
            
        } else if cNewPassword == nil || cNewPassword == "" {
            errorTitle = "Please enter confirm new password"
            
        } else if cNewPassword != newPassword {
            errorTitle = "New password and confirm new password are not the same"
        }
        
        if errorTitle != nil {
            Globals.showGenericErrorAlert(title: errorTitle, message: "")
            
        }else {
            if !APIRequest.isNetworkAvailable() {
                Globals.shared.showNoInternetConeectionAlert()
                return;
            }
            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.changePasswordRequest = APIRequest.changePassword(oldPassword:oldPassword! , newpPassword: newPassword!, cNewPassword: cNewPassword!) {(requets, response, error) in
                progressView.hide(animated: true)
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    Globals.showGenericAlert(title: "Your password has been changed successfully", message: "")
                    KeychainManager.shared.saveAccountDetailsToKeychain(account: (UserManager.shared.currentUser?.email)!, password: newPassword!)
                    self.navigationController?.popViewController(animated: true)
//                    self.onCancel()
//                    UserManager.logout()
                } else {
                    if let errorMessage = responseVO.errorMessage {
                        Globals.showGenericErrorAlert(title: errorMessage, message: "")
                    } else {
                        Globals.showGenericErrorAlert()
                    }
                }
            }
        }
    }
    
    func showErrorAlert(title: String?, message: String?) {
        Globals.showGenericErrorAlert(title: title, message: message)
    }

}
