//
//  UserCommentsViewController.swift
//  DavisImperial
//
//  Created by Surendra on 8/13/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD

class UserCommentsViewController: UIViewController {

    @IBOutlet var textView : UITextView!
    var apiRequest: APIRequest?
    var date: Date!

    static func getInstance() -> UserCommentsViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: "userCommentsViewController") as! UserCommentsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.layer.masksToBounds = true
        textView.layer.cornerRadius  = 20
        textView.layer.borderWidth   = 1
        textView.layer.borderColor   = UIColor(named: Color.darkGrayColor)?.cgColor
        textView.contentInset        = UIEdgeInsets(top: 8, left: 5, bottom: 8, right: 5)
        self.title = "Instructions"
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        let tapOnView = UITapGestureRecognizer(target: self, action: #selector(keyboardHide))
        self.view.addGestureRecognizer(tapOnView)
    }
    
    @objc func keyboardHide(){
        self.textView.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func onBookButtonTap(button: UIButton) {
        ServiceDS.shared.noRush = false
        book()
    }
    
    @IBAction func noRushPress(_ sender: Any) {
        ServiceDS.shared.noRush = true
        book()
    }
    
    func book() {
        let progressHud = MBProgressHUD.showAdded(to: self.view, animated: true)
        if !APIRequest.isNetworkAvailable() {
            Globals.shared.showNoInternetConeectionAlert()
            return;
        }
        ServiceDS.shared.instructions = textView.text
        self.apiRequest = APIRequest.bookServise(result: { (request, response, error) in
            progressHud.hide(animated: true)
            let responseVO = ResponseVO.init(response: response, error: error)
            if responseVO.isSuccess{
                Globals.showGenericAlert(title: "Service booked successfully", message: "", complition: {
                    NotificationCenter.default.post(name: UserManager.userDidLogin, object: nil)
                    ServiceDS.shared.clearService()
                })
            }else{
                Globals.showGenericAlert(title: responseVO.errorMessage, message: "")
            }
        })
    }
    
    @IBAction func onPressSchedule(_ sender: Any) {
        let vc = RequestPickupViewController.getInstance()
        vc.isDelivery = true
        ServiceDS.shared.noRush = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
