//
//  AlertController+Add.swift
//  DavisImperial
//
//  Created by Surendra on 17/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

var alertKey : String = "alertKey";

extension UIAlertController {
    
    var alertWindow : UIWindow? {
        set {
            objc_setAssociatedObject(self, &alertKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN);
        }
        get {
            return objc_getAssociatedObject(self, &alertKey) as! UIWindow?;
        }
    }
}

extension UIAlertController {

    func showAlert() {
        self.alertWindow = UIWindow.init(frame: UIScreen.main.bounds);
        self.alertWindow?.rootViewController = UIViewController.init();
        
        self.alertWindow?.tintColor = UIApplication.shared.delegate?.window??.tintColor;
        let topWindow = UIApplication.shared.windows.last;
        self.alertWindow?.windowLevel = (topWindow?.windowLevel)! + 1;
        
        self.alertWindow?.makeKeyAndVisible();
        self.alertWindow?.rootViewController?.present(self, animated: true, completion: nil);
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated);
        
        self.alertWindow?.isHidden = true;
        self.alertWindow = nil;
    }
    
    func dismissAlert() {
        weak var weakSelf = self;
        self.alertWindow?.rootViewController?.dismiss(animated: true, completion: { 
            weakSelf?.alertWindow?.isHidden = true;
            weakSelf?.alertWindow = nil;
        })
    }
    
    func isVisibleAlert() -> Bool {
        return self.alertWindow != nil;
    }
    
}
