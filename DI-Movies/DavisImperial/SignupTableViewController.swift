//
//  SignupTableViewController.swift
//  DavisImperial
//
//  Created by Surendra on 9/20/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class SignupTableViewController: UITableViewController {

    var signupVC : SignupViewController!
    
    static func getInstance(_ showCCInput : Bool) -> SignupTableViewController {
        let vc = SignupTableViewController.init()
        vc.signupVC = SignupViewController.getInstance()
        vc.addChild(vc.signupVC)
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.view.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        self.tableView.tableFooterView = UIView.init()
        self.tableView.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.signupVC.view;
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1;
    }
}
