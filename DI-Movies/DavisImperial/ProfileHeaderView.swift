//
//  ProfileHeaderView.swift
//  DavisImperial
//
//  Created by Surendra on 7/12/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView {

    @IBOutlet weak var profilePic : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var addressLabel : UILabel?
    @IBOutlet weak var backgroundView : UIView?
    @IBOutlet weak var completedBookingsCountLabel : UILabel?
    @IBOutlet weak var ratingCountLabel : UILabel?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2.0
        self.profilePic.layer.borderWidth = 1.0
        self.profilePic.layer.borderColor = UIColor.lightGray.cgColor
    }
}
