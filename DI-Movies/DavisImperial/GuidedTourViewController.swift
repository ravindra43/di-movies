//
//  GuidedTourViewController.swift
//  DavisImperial
//
//  Created by Surendra on 15/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

let defaultPageIndex = 0
let lastPageIndex = 3

let contactUsPageIndex = lastPageIndex

@objc protocol GuidedTourViewControllerDelegate {
    func onGuidedTourVisitCompleted(guidedTour : GuidedTourViewController, forSignin: Bool);
}

class GuidedTourViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    @IBOutlet weak var pageControl : UIPageControl!;
    @IBOutlet weak var nextButton : UIButton!;
    @IBOutlet weak var skipButton : UIButton!;
    @IBOutlet weak var skipButtonWidthConstant : NSLayoutConstraint?;
    
    var pageViewController : UIPageViewController!;
    var pages : Array<UIViewController> = [];
    var previousIndex : Int!;
    var skipButtonWidth : CGFloat!;
    var defaultSelectedIndex = 0
    
    weak var delegate : GuidedTourViewControllerDelegate? = nil;
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.previousIndex = defaultPageIndex;
        self.nextButton.layer.cornerRadius = self.nextButton.bounds.size.height/2.0;
        self.nextButton.layer.borderWidth = 1.0 / UIScreen.main.scale;
        self.nextButton.layer.borderColor = UIColor.white.cgColor
        if self.defaultSelectedIndex != 0 {
            self.nextButton.setTitle("SKIP", for: .normal)
        }
        // Pages
        for index in 0...3 {
            let guideStepVC = Globals.welcomeStoryboard().instantiateViewController(withIdentifier: "guideStepViewController") as! GuideStepViewController;
            guideStepVC.dictionary = self.dictionaryForPageContentAtIndex(index);
            
            self.pages.append(guideStepVC);
        }
        
        //PageViewController
        self.pageViewController.delegate = self;
        self.pageViewController.dataSource = self;
        self.pageViewController.view.backgroundColor = UIColor.clear;
        self.pageControl.numberOfPages = self.pages.count
        self.pageControl.currentPage = defaultSelectedIndex
        self.setupViewControllerAtIndex(defaultSelectedIndex);
    }
    
//    override var prefersStatusBarHidden: Bool {
//        return f;
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.pageViewController = segue.destination as? UIPageViewController;
    }
    
    func dictionaryForPageContentAtIndex(_ index : Int) -> [String : String] {
        var dict : [String : String]!;
        switch index {
        case 0:
            dict = [titleKey : "WELCOME", imageKey : "walkthrough1", descriptionKey : "Since 1956 our family has been providing couture garment care to Chicago’s most discriminating clientele. We are excited to introduce the convenience of mobile pickup requests to our existing clients. If you do not have an active account please call our office to setup @ 866.267.4560."];
        case 1:
            dict = [titleKey : "SERVICE AREA", imageKey : "walkthrough2", descriptionKey : "Our bonded professional client sales representatives provide pick-up and delivery service to clients from Hyde Park to the South Loop, Gold Coast, Lincoln Park, north to Lake Forest, and west to Barrington. FabricareByMail™ service available nationwide upon request."];
        case 2:
            dict = [titleKey : "SCHEDULE APPOINTMENT", imageKey : "walkthrough3", descriptionKey : "We will make every effort to accommodate all requests made for same day pickups placed before 9 A.M. However, please wait for confirmation as this will largely depend on the days service area. Feel free to call our office at anytime 866.267.4560 or email info@davisimperial.com"];
        case 3:
            dict = [titleKey : "", imageKey : "walkthrough4", descriptionKey : "3325 W. Bryn Mawr Ave Chicago, IL 60659\n1920 Harrison St Evanston, IL 60201\nwww.davisimperial.com\n866.267.4560"];
        default: break;
        }
        return dict;
    }
    
    func viewControllerAtIndex(_ index : Int) -> UIViewController? {
        if (self.pages.count == 0 || index >= self.pages.count) {
            return nil;
        }
        
        return self.pages[index];
    }
    
    func animateButtonsForGuideStepAtIndex(_ index : Int) {
        return
        /*var rect = self.nextButton.frame;
        if (index == guidedTourPageThird) {
            rect.size.width = self.nextButton.frame.size.width + self.skipButtonWidth;
            
            self.skipButtonWidthConstant?.constant = 0;
            UIView.animate(withDuration: 0.4, animations: {
                self.nextButton.setTitle("Start", for: UIControlState.normal);
                self.nextButton.frame = rect;
                self.skipButton.alpha = 0;
            })
        } else if (self.skipButton.frame.size.width < self.skipButtonWidth) {
            rect.size.width = self.nextButton.frame.size.width - self.skipButtonWidth;
            
            self.skipButtonWidthConstant?.constant = self.skipButtonWidth;
            UIView.animate(withDuration: 0.4, animations: {
                self.nextButton.setTitle("Next", for: UIControlState.normal);
                self.nextButton.frame = rect;
            }, completion: { (complete : Bool) in
                self.skipButton.alpha = 1;
            })
        }*/
    }
    
    func setupViewControllerAtIndex(_ index : Int) {
        if let vc = self.viewControllerAtIndex(index) {
            self.pageViewController.setViewControllers([vc], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil);
        }
    }
    
    @IBAction func onSignInButtonTap() {
        self.guidedTourCompleted(signin: true);
    }
    
    @IBAction func onSignupButtonTap() {
        self.guidedTourCompleted(signin: false);
    }

    @IBAction func onTapNextButton() {
        let currentIndex = self.pageControl.currentPage;
        if (currentIndex < lastPageIndex) {
            let nextIndex = currentIndex + 1;
            self.animateButtonsForGuideStepAtIndex(nextIndex);
            self.setupViewControllerAtIndex(nextIndex);
            self.pageControl.currentPage = nextIndex;
        } else {
            self.guidedTourCompleted();
        }
    }
    
    @IBAction func onTapPageControl() {
        let currentIndex = self.pageControl.currentPage;
        let currentVC : UIViewController! = self.viewControllerAtIndex(currentIndex);
        self.animateButtonsForGuideStepAtIndex(currentIndex);
        if (currentIndex < self.previousIndex) {
            self.pageViewController.setViewControllers([currentVC], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil);
        } else {
            self.pageViewController.setViewControllers([currentVC], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil);
        }
        self.previousIndex = currentIndex;
    }
    
    @IBAction func onTapSkipButton() {
        self.guidedTourCompleted();
    }
    
    func guidedTourCompleted() {
        self.delegate?.onGuidedTourVisitCompleted(guidedTour: self, forSignin: true);
    }
    
    func guidedTourCompleted(signin : Bool) {
        self.delegate?.onGuidedTourVisitCompleted(guidedTour: self, forSignin: signin);
    }
    
//MARK:- PageViewControllerDelegate
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = 0;
        if (self.pages.contains(viewController)) {
            index = self.pages.firstIndex(of: viewController)!;
        }
        index += 1;
        if (index == self.pages.count) {
            return nil;
        }
        
        return self.viewControllerAtIndex(index);
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = 0;
        if (self.pages.contains(viewController)) {
            index  = self.pages.firstIndex(of: viewController)!;
        }
        if (index == 0 || index == NSNotFound) {
            return nil;
        }
        
        index -= 1;
        return self.viewControllerAtIndex(index);
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let controller = self.pageViewController.viewControllers?[0];
        if (finished && completed && controller != nil) {
            if (self.pages.contains(controller! )) {
                let index : Int! = self.pages.firstIndex(of: controller!);
                self.animateButtonsForGuideStepAtIndex(index);
                self.pageControl.currentPage = index;
            }
        }
    }
    
}
