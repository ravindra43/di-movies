//
//  NetworkService.swift
//  DavisImperial
//
//  Created by Surendra on 17/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import Foundation
import Alamofire


enum APIResponseType {
    case XML;
    case RawData;
    case JSON;
}

protocol NetworkServiceDelegate : NSObjectProtocol{
    func networkService(service : NetworkService, response : Any?, error : Error?);
}

class NetworkService {
    
    var dataTask : DataRequest?;
    var headers : HTTPHeaders?;
    var params = [String: Any]();
    var body : Data?;
    let url : String!;
    var enableLogging = false
    var httpResponseCode : Int?
    
    var responseType : APIResponseType = APIResponseType.JSON;
    
    weak var delegate : NetworkServiceDelegate?;
    
    init(WithURL URL: String, delegate : NetworkServiceDelegate) {
        self.url = URL;
        self.delegate = delegate;
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded", "Accept" : "application/json"]
    }
    
    static func isNetworkAvailable() -> Bool {
        return (Alamofire.NetworkReachabilityManager()?.isReachable)!;
    }
    
//    static func checkNetworkConnectionWith(completion : ((_ isAvailable : Bool) -> Void)?) {
//        if (Alamofire.NetworkReachabilityManager()?.networkReachabilityStatus != .unknown) {
//            completion?(self.isNetworkAvailable());
//        } else {
//            Alamofire.NetworkReachabilityManager()?.listener = {(status) in
//                completion?(NetworkService.isNetworkAvailable());
//                Alamofire.NetworkReachabilityManager()?.listener = nil;
//            }
//        }
//    }
    static func checkNetworkConnectionWith(completion : ((_ isAvailable : Bool) -> Void)?) {
        if (Alamofire.NetworkReachabilityManager()?.status != .unknown) {
            completion?(self.isNetworkAvailable());
        } else {
            Alamofire.NetworkReachabilityManager()?.startListening(onUpdatePerforming: {(status) in
                switch status{
                    case .unknown:
                        print("Unknown network reachability")
                    case .notReachable:
                        print("Not reachable network")
                    case .reachable(.cellular),.reachable(.ethernetOrWiFi):
                        print("Reachable network")
                }
            })
        }
    }
    
    func cancel() {
        self.dataTask?.cancel();
    }
    
    func setHeaderDict(dict : HTTPHeaders) {
        self.headers = dict;
    }
    
    func setBody(data : Data) {
        self.body = data;
    }
    
    func sendGETRequest() {
        self.sendRequestWithMethod("GET");
    }
    
    func sendPOSTRequest() {
        self.sendRequestWithMethod("POST");
    }
    
    func sendPUTRequest() {
        self.sendRequestWithMethod("PUT");
    }
    
    func sendDeleteRequest() {
        self.sendRequestWithMethod("DELETE");
    }
    
    func sendRequestWithMethod(_ method : String) {
        var m : HTTPMethod = .get
        if method == "POST" {
            m = .post
        } else if method == "PUT" {
            m = .put
        }else if method == "DELETE" {
            m = .delete
        }

//        if self.enableLogging {
//            print("Requesting: \(method) \(self.url) params: \(self.params)")
//        }
        if self.headers?["Content-Type"] == "application/json" {
            self.dataTask = AF.request(self.url, method: m, parameters: self.params,encoding:JSONEncoding.default , headers: self.headers)
        } else {
            self.dataTask = AF.request(self.url, method: m, parameters: self.params, headers: self.headers)
        }
        
        self.dataTask?.response(completionHandler: { (response) in
            if self.enableLogging {
                print("Request served: \(method) \(self.url ?? "URL NOT FOUND") params: \(self.params)")
                var responseLogData = response.data == nil ? "nil" : self.getResponseFromData(data: response.data)
                if responseLogData == nil {
                    responseLogData = String(describing: String.init(data: response.data!, encoding: String.Encoding.utf8))
                } else {
                    let data = try? JSONSerialization.data(withJSONObject: responseLogData!, options: JSONSerialization.WritingOptions(rawValue: 0))
                    let str = "Raw Response\n\n\n" + String.init(data: data!, encoding: String.Encoding.utf8)! + "\n\n\n >>>>>>>>>>>>>>>>>"
                    print(str)
                }
                print("Response: \(String(describing: responseLogData)) error: \(String(describing: response.error))")
                
            }
            if response.response != nil {
                self.httpResponseCode = response.response!.statusCode
            }
            self.delegate?.networkService(service: self, response: self.getResponseFromData(data: response.data), error: response.error);
        })
    }
    
    func getResponseFromData(data: Data?) -> Any? {
        guard let resData = data else {
            return nil;
        }
        
        var response : Any? = nil;
        switch self.responseType {
        case .XML:
            response = XMLURLParser.sharedInstance().dictionary(with: resData);
        case .JSON:
            response = try? JSONSerialization.jsonObject(with: resData, options: .allowFragments) as! [String: Any];
        default:
            response = String(data: resData, encoding: String.Encoding.utf8);
        }
        
        return response;
    }
    
}
