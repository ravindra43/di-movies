//
//  BookingsViewController.swift
//  Uncle Fitter Mechanic
//
//  Created by Surendra on 6/30/17.
//  Copyright © 2017 Surendra. All rights reserved.
//

import UIKit
import MBProgressHUD

class BookingsViewController: UIViewController, BookingTableViewCellDelegate {

    var carTrimId : UInt64?
    var showBack = false
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var noItemsLabel : UILabel!

    var refreshControl = UIRefreshControl.init()
    var progressHud : MBProgressHUD?
    
    var needsToRefresh = false
    
    fileprivate var bookingDataSource : BookingsDataSource!
    
    static func getInstance() -> BookingsViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: "bookingsViewController") as! BookingsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.title = "My Bookings"
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.rowHeight = UITableView.automaticDimension
        self.refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tableView.addSubview(self.refreshControl)
        
        self.bookingDataSource = BookingsDataSource.init(success: { [weak self] () -> Void in self?.itemsLoaded()}, failure: { [weak self] () -> Void in self?.itemLoadFailed()})
        self.showLoading()
        self.bookingDataSource.reloadData()
        if showBack {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(onCancel))
        }
    }
    
    @objc func onCancel(){
        NotificationCenter.default.post(name: UserManager.userDidLogin, object: nil)
        return
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigation(hide: false)
        if self.needsToRefresh {
            self.showLoading()
            self.bookingDataSource.reloadData()
        }
    }
    
    func onBackBarButtonTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func onBookingStatusChangedNotification() {
        self.needsToRefresh = true
    }
    
    @objc func onRefresh(){
        self.noItemsLabel.isHidden = true
        self.bookingDataSource.reloadData()
    }
    
    private func itemsLoaded()  {
        self.refreshControl.endRefreshing()
        self.hideLoading()
        self.tableView.reloadData()
        self.needsToRefresh = false
        self.noItemsLabel.text = "No bookings found."
        self.noItemsLabel.isHidden = self.bookingDataSource.items.count != 0
    }
    
    private func itemLoadFailed()  {
        self.refreshControl.endRefreshing()
        self.hideLoading()
        self.bookingDataSource.items = []
        self.tableView.reloadData()
        self.noItemsLabel.text = "Something went wrong. Couldn't load bookings."
        self.noItemsLabel.isHidden = false;
    }
    
    fileprivate func showLoading(){
        self.progressHud = MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    fileprivate func hideLoading(){
        self.progressHud?.hide(animated: true)
    }
    
    func onBookingActionButtonTap(cell: BookingTableViewCell, accept: Bool) {
        if !APIRequest.isNetworkAvailable() {
            Globals.shared.showNoInternetConeectionAlert()
            return;
        }
        let booking = self.bookingDataSource.items[(self.tableView.indexPath(for: cell)?.row)!] as BookingVO
        
        Globals.showConfirmationAlert(title: "Confirm", message: "Are you sure you want to \(accept ? "Accept" : "Reject")?", cancelButtonTitle: "No", actionButtonTitle: "Yes") { 
            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
            _ = APIRequest.respondToBooking(bookingId: booking.bookingId, accept: accept, result: { (request, response, error) in
                progressView.hide(animated: true)
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    Globals.showGenericAlert(title: "Booking \(accept ? "Accepted" : "Rejected")", message: "")
                    self.onRefresh()
                    
                } else {
                    if let errorMessage = responseVO.errorMessage {
                        Globals.showGenericErrorAlert(title: errorMessage, message: "")
                        
                    } else {
                        Globals.showGenericErrorAlert()
                    }
                }
            })
        }
    }
}

extension BookingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.bookingDataSource.status == .loading ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookingDataSource.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookingCell", for: indexPath) as! BookingTableViewCell
        cell.delegate = self
        cell.setBooking(booking: self.bookingDataSource.items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if scrollView.shouldLoadMoreItems() {
            if self.bookingDataSource.status != .failed && self.bookingDataSource.status != .loading && !self.bookingDataSource.allItemsLoaded {
                self.bookingDataSource.loadNextPage()
            }
        }
    }
}
