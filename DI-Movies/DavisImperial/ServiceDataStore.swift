//
//  ServiceDataStore.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 13/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import Foundation

enum PickupType: String {
    case asap
    case noRush
    case schedule
}
class ServiceDS {
    
    static var shared: ServiceDS = ServiceDS()
    
    fileprivate var type: PickupType    = PickupType.asap
    fileprivate var date: String?       = nil
    fileprivate var scheduletime: String?       = nil
    fileprivate var delivery: String?   = nil
    fileprivate var deliverytime: String?   = nil
    fileprivate var lat: Double?        = nil
    fileprivate var long: Double?       = nil
    fileprivate var add:String?         = nil
    fileprivate var bags:Int?           = nil
    fileprivate var barArray: [String]? = nil
    fileprivate var instruct: String?   = nil
    fileprivate var rush: Bool?         = nil

    var scheduleType: PickupType {
        get{ return type }
        set{
            type = newValue
        }
    }
    
    var scheduleDate: String? {
        get { return date }
        set {
            date = newValue
        }
    }
    var scheduleTime: String? {
        get { return scheduletime }
        set {
            scheduletime = newValue
        }
    }
    
    var deliveryDate: String? {
        get { return delivery }
        set {
            delivery = newValue
        }
    }
    var deliveryTime: String? {
        get { return deliverytime }
        set {
            deliverytime = newValue
        }
    }
    var location: (Double?, Double?) {
        get { return (lat, long) }
        set {
            lat  = newValue.0
            long = newValue.1
        }
    }
    
    var address: String? {
        get {return add}
        set {
            add = newValue
        }
    }
    
    var numberOfBags: Int? {
        get { return bags }
        set {
            bags = newValue
        }
    }
    
    var barCodes: [String]? {
        get {return barArray}
        set {
            barArray = newValue
        }
    }
    
    var noRush: Bool?{
        get {return rush}
        set {
            rush = newValue
        }
    }
    
    var instructions: String? {
        get {return instruct}
        set {
            instruct = newValue
        }
    }
    
    func clearService() {
        scheduleDate = nil
        location     = (nil, nil)
        address      = nil
        numberOfBags = nil
        instructions = nil
        barArray?.removeAll()
    }
}



