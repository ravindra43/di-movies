//
//  BagSelectionViewController.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 12/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import UIKit

class BagSelectionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var enterNumberView: DIView!
    @IBOutlet weak var superView: DIView!
    @IBOutlet weak var numberOfBagsLabel: UILabel!
    @IBOutlet weak var pickerHeight: NSLayoutConstraint!
    var bagArray = [String]()
    var numberOfbags = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapOnnumberOfBags = UITapGestureRecognizer(target: self, action: #selector(onPressNumberOfbags))
        enterNumberView.addGestureRecognizer(tapOnnumberOfBags)
        pickerHeight.constant = 0
        superView.alpha = 0
        bagArray = getArray(start: 1, end: 36)
        pickerView.selectedRow(inComponent: 0)
        pickerView.setValue(UIColor(named: Color.blackColor), forKey: "textColor")
        self.title = "Number of Inventory Sheets"
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        ServiceDS.shared.numberOfBags = 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bagArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return bagArray[row]
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @objc func onPressNumberOfbags(){
        UIView.animate(withDuration: 0.3) {
            self.superView.alpha = self.superView.alpha == 0 ? 1 : 0
            
            self.pickerHeight.constant = self.pickerHeight.constant == 250 ? 0 : 250
            self.view.layoutIfNeeded()
        }
    }
    func getArray(start: Int, end: Int) -> [String] {
        var array = [String]()
        for value in start ... end {
            array.append(String(value))
        }
        return array
    }
    
    static func getInstance() -> BagSelectionViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTBagSelectionVCID) as! BagSelectionViewController
    }
    
    @IBAction func onPressNext(_ sender: Any) {
        if ServiceDS.shared.numberOfBags != nil {
            let vc = BarcodeScannerViewController.getInstance()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onTapSkip(_ sender: Any) {
        ServiceDS.shared.numberOfBags = nil
        let vc = UserCommentsViewController.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        numberOfbags            = Int(bagArray[row])!
        numberOfBagsLabel.text  = bagArray[row] + (numberOfbags > 1 ? " Bags" : " Bag")
        ServiceDS.shared.numberOfBags = numberOfbags
    }
    deinit {
       ServiceDS.shared.numberOfBags = nil
    }
}
