//
//  Color.swift
//  DavisImperial
//
//  Created by Surendra on 16/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class Color {
    
    static let blackColor = "blackColor"
    static let buttonBackgroundColor = "buttonBackgroundColor"
    static let buttonTextColor = "buttonTextColor"
    static let clearColor = "clearColor"
    static let darkGrayColor = "darkGrayColor"
    static let dullWhiteColor = "dullWhiteColor"
    static let grayColor = "grayColor"
    static let greenColor = "greenColor"
    static let invertColor = "invertColor"
    static let messageRecieverColor = "messageRecieverColor"
    static let messageSenderColor = "messageSenderColor"
    static let navbarColor = "navbarColor"
    static let pageBackgroundColor = "pageBackgroundColor"
    static let redColor = "redColor"
    static let textFieldBackgroundColor = "textFieldBackgroundColor"
    static let textLabelColor = "textLabelColor"
    static let whiteColor = "whiteColor"

    static func blackOverlay() -> UIColor {
        return UIColor.black.withAlphaComponent(0.2);
    }

}

//class Color {
//
//    static let blackColor = "blackColor"
//    static let buttonBackgroundColor = "buttonBackgroundColor"
//    static let buttonTextColor = "buttonTextColor"
//    static let clearColor = "clearColor"
//    static let darkGrayColor = "darkGrayColor"
//    static let dullWhiteColor = "dullWhiteColor"
//    static let grayColor = "grayColor"
//    static let greenColor = "greenColor"
//    static let invertColor = "invertColor"
//    static let messageRecieverColor = "messageRecieverColor"
//    static let messageSenderColor = "messageSenderColor"
//    static let navbarColor = "navbarColor"
//    static let pageBackgroundColor = "pageBackgroundColor"
//    static let redColor = "redColor"
//    static let textFieldBackgroundColor = "textFieldBackgroundColor"
//    static let textLabelColor = "textLabelColor"
//    static let whiteColor = "whiteColor"
//
//    static func mainColor() -> UIColor {
//        var color = "#C12226";
//        return Color.colorWithHex(hex: &color, alpha: 0.79)!;
//    }
//
//    static func brandColor() -> UIColor {
//        var color = "#E63C24";
//        return Color.colorWithHex(hex: &color, alpha: 0.79)!;
//    }
//
//    static func lightBlueColor() -> UIColor {
//        var color = "#FBFFFF";
//        return Color.colorWithHex(hex: &color, alpha: 0.79)!;
//    }
//
//    static func lightGreenColor() -> UIColor {
//        var color = "#E8FFED";
//        return Color.colorWithHex(hex: &color, alpha: 0.79)!;
//    }
//
//    static func otherBrandColor() -> UIColor {
//        var color = "#007AFF";
//        return Color.colorWithHex(hex: &color, alpha: 0.79)!;
//    }
//
//    static func navBarColor() -> UIColor {
//        return UIColor.init(white: 0.2, alpha: 1.0)
//    }
//
//    static func pageBackgroundColors() -> UIColor {
//        return UIColor.init(white: 0.98, alpha: 1);
//    }
//
//    static func blackOverlay() -> UIColor {
//        return UIColor.black.withAlphaComponent(0.2);
//    }
//
//    static func gray() -> UIColor {
//        var color = "#A8A8A8";
//        return Color.colorWithHex(hex: &color, alpha: 1.0)!;
//    }
//
//    static func textGray() -> UIColor {
//        var color = "#A7A7A7";
//        return Color.colorWithHex(hex: &color, alpha: 1.0)!;
//    }
//    static func black() -> UIColor {
//        var color = "#000000";
//        return Color.colorWithHex(hex: &color, alpha: 1.0)!;
//    }
//
//    static func white() -> UIColor {
//        var color = "#FFFFFF";
//        return Color.colorWithHex(hex: &color, alpha: 1.0)!;
//    }
//
//    static func colorWithHex(hex: inout String, alpha : Float) -> UIColor? {
//        if (hex.isEmpty) { return nil };
//
//        if (hex.hasPrefix("#")) {
//            hex.remove(at: hex.startIndex)
//        }
//
//        if ((hex.count) != 6) {
//            return Color.mainColor();
//        }
//
//        var rgbValue:UInt32 = 0
//        Scanner(string: hex).scanHexInt32(&rgbValue)
//
//        return UIColor(
//            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
//            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
//            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
//            alpha: CGFloat(1.0)
//        )
//    }
//}
