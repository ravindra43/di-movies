//
//  UserVO.swift
//  DavisImperial
//
//  Created by Surendra on 17/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import Foundation

class UserVO : NSObject, NSCoding {
    
    var firstName: String?;
    var lastName: String?;
    var email: String?;
    var phone: String?;
//    var phoneCountryCode: String?;
    var userId: String!
    var productionName: String?;
    var position: String?;
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.userId, forKey: "userId");
        aCoder.encode(self.firstName, forKey: "first_name");
        aCoder.encode(self.lastName, forKey: "last_name");
        aCoder.encode(self.email, forKey: "email");
        aCoder.encode(self.phone, forKey: "phone");
        aCoder.encode(self.productionName, forKey: "production_name");
        aCoder.encode(self.position, forKey: "postion");
    }
    
    init(userId: String,  firstName: String, lastName: String, email: String, phone: String, production: String, position: String) {
        super.init();
        self.userId = userId
        self.firstName = firstName;
        self.lastName = lastName;
        self.email = email;
        self.phone = phone;
        self.productionName = production;
        self.position = position;
    }
    
//    func addressString() -> String {
//        var a = [String]()
//        if self.address != nil {
//            a.append(self.address!)
//        }
//        if self.city != nil {
//            a.append(self.city!)
//        }
//        return a.joined(separator: "\n")
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.userId = aDecoder.decodeObject(forKey: "userId") as? String;
        self.firstName = aDecoder.decodeObject(forKey: "first_name") as? String;
        self.lastName = aDecoder.decodeObject(forKey: "last_name") as? String;
        self.email = aDecoder.decodeObject(forKey: "email") as? String;
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String;
        self.productionName = aDecoder.decodeObject(forKey: "production_name") as? String;
        self.position = aDecoder.decodeObject(forKey: "position") as? String;
    }
}

extension UserVO {
    convenience init?(json: [String: Any]) {
        guard let userid = json["_id"] as? String,
            let first = json["first_name"] as? String,
            let last = json["last_name"] as? String,
            let emailid = json["email"] as? String,
            let phoneNo = json["phone"] as? NSNumber,
            let productionname = json["production_name"] as? String,
            let position = json["position"] as? String
            else {
                return nil
        }
        let noString  = phoneNo.stringValue
        self.init(userId: userid, firstName: first, lastName: last, email: emailid, phone: noString, production: productionname, position: position)
    }
}

