//
//  Globals.swift
//  DavisImperial
//
//  Created by Surendra on 17/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

let kLTLeftMainMenuWidth: CGFloat = 260.0;
let kLTMainMenuAnimationDuration: CGFloat = 0.2;

//Storyboard ID
let kLTLoginVCID            = "loginViewControllerID";
let kLTSignupVCID           = "signupViewController";
let kLTChangePasswordVCID   = "changePasswordViewController"
let kLTNewMessageVCID       = "newMessageViewController"
let kLTMainMenuVCID         = "leftMenuViewControllerID";
let kLTUserProfileVCID      = "userProfileViewController";
let kLTGuidedTourVCID       = "guidedTourViewController";
let kLTPickUpSelectVCID     = "pickUpSelectionViewController"
let kLTlocationVCID         = "locationViewController"
let kLTOptionVCID           = "LocationOptionViewController"
let kLTBagSelectionVCID     = "bagSelectionViewController"
let kLTQRCodeVCID           = "BarcodeScannerViewController"
let diAccountKey            =  "lastAccessedUserName";

let modeDebug = true

class Globals {

    private var noInternetAlert : UIAlertController?;
    
    static let shared : Globals = Globals();
    
    private init() {}

    static func isProd() -> Bool {
        return true;
    }
    
    static func isIPad() -> Bool {
        return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad;
    }

    static func showNoInternetConeectionAlertIfNeeded() -> Bool{
        if !APIRequest.isNetworkAvailable() {
            Globals.shared.showNoInternetConeectionAlert()
            return true
        }
        return false
    }
    
    func showNoInternetConeectionAlert() {
        self.showNoInternetAlertWithCompletion(completion: nil);
    }
    
    func showNoInternetAlertWithCompletion(completion : (() -> Void)?) {
        if (self.noInternetAlert == nil || !(self.noInternetAlert?.isVisibleAlert())!) {
            self.noInternetAlert = UIAlertController.init(title: "No Internet Connection", message:"Please check your internet connection", preferredStyle: UIAlertController.Style.alert);
            self.noInternetAlert?.addAction(UIAlertAction.init(title: "OK", style: UIAlertAction.Style.cancel, handler: { (action) in
                completion?();
            }))
            self.noInternetAlert?.showAlert();
        }
    }
    
    static func showGenericErrorAlert() {
        self.showGenericErrorAlert(title: "Something went wrong!", message: "Please try later.")
    }
    
    static func showGenericErrorAlert(title:String?, message: String?) {
        self.showGenericErrorAlert(title: title, message: message, cancelButtonTitle: "Dismiss")
    }
    
    static func showGenericAlert(title:String?, message: String?) {
        let alert = UIAlertController.init(title: title, message:message, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction.init(title: "Dismiss", style: UIAlertAction.Style.cancel, handler: nil))
        alert.showAlert();
    }
    
    static func showGenericAlert(title:String?, message: String?, complition: @escaping () -> Void) {
        let alert = UIAlertController.init(title: title, message:message, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert) in
            complition()
        }))
        
        alert.showAlert();
    }
    
    static func showGenericAlert(title:String?, complition: @escaping (_ text: String) -> Void) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        alert.addTextField { (textField) in
            
        }
        let textField = alert.textFields![0] // Force unwrapping because we know it exists.
        textField.keyboardType = .numberPad
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            complition(textField.text!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.showAlert();
    }
    
    static func showGenericErrorAlert(title:String?, message: String?, cancelButtonTitle: String?) {
        let alert = UIAlertController.init(title: title, message:message, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle, style: UIAlertAction.Style.cancel, handler: nil))
        alert.showAlert();
    }
    
    static func showConfirmationAlert(title:String?, message: String?, cancelButtonTitle: String?, actionButtonTitle: String?, onConfirm : @escaping () -> Void) {
        let alert = UIAlertController.init(title: title, message:message, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle == nil ? "No" : cancelButtonTitle, style: UIAlertAction.Style.cancel, handler: nil))
        alert.addAction(UIAlertAction.init(title: actionButtonTitle == nil ? "Yes" : actionButtonTitle, style: .default, handler: { (action) in
            onConfirm()
        }))
        alert.showAlert();
    }
    
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isValidPhone(value: String) -> Bool {
        /*let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)*/
        return value.count > 3 && self.isNumber(value:value)
    }
    
    static func isNumber(value: String) -> Bool {
        let a = Int64(value)
        return a != nil
    }
    
    static func getUnsignedNum(object: Any?) -> UInt16 {
        if let _objNum = object as? UInt16 {
            return _objNum
        }
        
        if let str = object as? String {
            if let num = UInt16(str.trim()) {
                return num
            }
        }
        
        return 0
    }
    
    static func formatNumber(number: NSNumber) -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: number)
        return formattedNumber
    }
    
    static func formatPrice(price:Any?) -> String {
        var priceStr = "0.0"
        if let str = price as? String {
            priceStr = str
        } else if let num = price as? Int {
            priceStr = "\(num)"
        }
        let priceFormatter = NumberFormatter.init()
        priceFormatter.numberStyle = .currency
        priceFormatter.locale = Locale.init(identifier: "ak_GH")
        return priceFormatter.currencySymbol + priceStr
    }
    
    static func mainStoryboard() -> UIStoryboard{
        return UIStoryboard.init(name: "Main", bundle: Bundle.main);
    }
    static func welcomeStoryboard() -> UIStoryboard{
        return UIStoryboard.init(name: "Welcome", bundle: Bundle.main);
    }
    
    static func backBarButton(target: Any?, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem.init(image: UIImage.init(named: "topbarBack"), style: .plain, target: target, action: action)
    }
    
    static func hideBottomLine(navBar: UIView?) {
        self.findHairlineImageViewUnder(view: navBar)?.isHidden = true
        if let bar = navBar as? UINavigationBar {
            bar.shadowImage = UIImage.init()
        }
    }
    
    static func findHairlineImageViewUnder(view : UIView?) -> UIImageView? {
        if view is UIImageView && (view?.bounds.size.height)! <= CGFloat(1.0) {
            return view as? UIImageView
        }
        for subview in (view?.subviews)! {
            let imgv = self.findHairlineImageViewUnder(view: subview)
            if imgv != nil {
                return imgv
            }
        }
        return nil
    }
    
    static func dateFromString(string: String?) -> Date? {
        if let dateString = string {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
            dateFormatter.timeZone = TimeZone(secondsFromGMT: secondsFromGMT)
            let date = dateFormatter.date(from: dateString)
            return date
        }
        return nil
    }

    static func formattedDate(date: Date?) -> String? {
        if date != nil {
            let inputDateFormatter = DateFormatter()
            inputDateFormatter.dateFormat = "MM-dd-yyyy"
            return inputDateFormatter.string(from: date!)
        }
        return nil
    }
    
    static func formattedDateTime(date: Date?) -> String? {
        if date != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy hh:mm aaa"
            return dateFormatter.string(from: date!)
        }
        return nil
    }
    static func formattedForLocalDateTime(date: Date?) -> String? {
        if date != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
            dateFormatter.timeZone = TimeZone(identifier: "GMT")
            return dateFormatter.string(from: date!)
        }
        return nil
    }
}

// MARK: NAVIGATION BACK BUTTON SETUP
extension UINavigationController {
    func setNormalBackButton(color: UIColor, image: UIImage?) {
        self.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        self.navigationBar.shadowImage = image
        self.navigationBar.isTranslucent = true
        self.navigationBar.tintColor = color
        self.navigationBar.barStyle = .default
    }
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 18)!, NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    func setNavigation(hide:Bool) {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.shadowColor = .clear
            self.navigationBar.tintColor = .white
            self.navigationBar.isTranslucent = true
            if hide {
                navBarAppearance.backgroundColor = .clear
                navigationBar.standardAppearance = navBarAppearance;
                navigationBar.scrollEdgeAppearance = navBarAppearance;
                navigationBar.compactAppearance = navBarAppearance;
            }else{
                navBarAppearance.backgroundColor = UIColor(named: Color.navbarColor)
                navigationBar.standardAppearance = navBarAppearance;
                navigationBar.scrollEdgeAppearance = navBarAppearance;
                navigationBar.compactAppearance = navBarAppearance;
            }
        }
    }
}

extension UIViewController {
    func emptyBackBarButton() -> UIBarButtonItem {
        return UIBarButtonItem.init(title: "", style: .plain, target: nil, action: #selector(onPressBackButton))
    }
    @objc func onPressBackButton(){
        self.navigationController?.closeRight()
        self.navigationController?.popViewController(animated: true)
    }
    func hideNavigationBar() {
        if #available(iOS 13, *) {
            self.navigationController?.setNavigation(hide: true)
        }else{
            self.navigationController?.setNormalBackButton(color: .white, image: UIImage())
        }
    }
    func showNavigationBar() {
        if #available(iOS 13, *) {
            self.navigationController?.setNavigation(hide: false)
        }else{
            self.navigationController!.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
            self.navigationController!.navigationBar.shadowImage = nil
        }
    }
}
