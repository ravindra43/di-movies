//
//  BookingTableViewCell.swift
//  Uncle Fitter Mechanic
//
//  Created by Surendra on 7/1/17.
//  Copyright © 2017 Surendra. All rights reserved.
//

import UIKit

@objc protocol BookingTableViewCellDelegate {
    func onBookingActionButtonTap(cell : BookingTableViewCell, accept: Bool);
}

class BookingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var statusLabel : UILabel!
    @IBOutlet weak var bookingDateLabel : UILabel!
    @IBOutlet weak var proposeDateLabel : UILabel!
    @IBOutlet weak var bookingIdLabel : UILabel!
    @IBOutlet weak var buttonBackView : UIView!
    @IBOutlet weak var buttonBackViewHeight : NSLayoutConstraint!
    @IBOutlet weak var DeliveryDateLabel: UILabel!
    
    weak var delegate : BookingTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.buttonBackView.isHidden = true
        self.buttonBackViewHeight.constant = 0;
    }
    
    override func prepareForReuse() {
        self.proposeDateLabel.text = ""
        self.statusLabel.text = ""
        self.bookingIdLabel.text = ""
        self.bookingDateLabel.text = ""
    }
    
    func setBooking(booking: BookingVO) {
        if booking.bookingDate != nil {
            self.bookingDateLabel.text = "Booking Date: " + Globals.formattedDateTime(date: booking.bookingDate)!
        }
        let isSchedule = booking.serviceType == PickupType.schedule && booking.proposeDate != nil
        let isDeliverySchedule = booking.deliveryType != nil && booking.deliveryType == PickupType.schedule
        
        
        var schedule_date: String? {
            if booking.proposeDate != nil {
                return booking.proposeTime != nil ? Globals.formattedDate(date: booking.proposeDate) : Globals.formattedDateTime(date: booking.proposeDate)
            }
            return  nil;
        }
        
        var delivery_date: String? {
            if booking.bookingDate != nil {
                return booking.deliveryTime != nil ? Globals.formattedDate(date: booking.deliveryDate) : Globals.formattedDateTime(date: booking.bookingDate);
            }
            return nil
        }
        
        let schedule_time = booking.proposeTime != nil ? ", \(booking.proposeTime ?? "")" : ""
        let delivery_time = booking.deliveryTime != nil ? ", \(booking.deliveryTime ?? "")" : ""

        self.proposeDateLabel.text = "Schedule Date: " + (isSchedule ?  (schedule_date ?? "") + schedule_time  : "ASAP")
        self.DeliveryDateLabel.text = "Delivery Date: " + (booking.noRush != nil && booking.noRush! ? "No Rush" : isDeliverySchedule ?  (delivery_date ?? "") + delivery_time : "ASAP")
        if booking.statusString() == "Accepted" {
            self.statusLabel.textColor = UIColor(named: Color.greenColor)
        }
        else if booking.statusString() == "Rejected" {
            self.statusLabel.textColor = UIColor(named: Color.redColor)
        }
        else {
            self.statusLabel.textColor = UIColor(named: Color.textLabelColor)
        }
        self.statusLabel.text = booking.statusString()
        self.bookingIdLabel.text = "Booking Id: " + booking.bookingDisplayId
    }
    

    @IBAction func onRejectButtonTap() {
        self.delegate?.onBookingActionButtonTap(cell: self, accept: false)
    }
}
