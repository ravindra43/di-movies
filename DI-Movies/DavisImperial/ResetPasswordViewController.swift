//
//  LoginViewController.swift
//  DavisImperial
//
//  Created by Surendra on 12/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD

class ResetPasswordViewController: KeyboardReadyViewController {
    
    @IBOutlet weak var usernameTextField : UITextField!; // email
    var apiRequest : APIRequest?

    static func getInstance() -> ResetPasswordViewController {
        return Globals.welcomeStoryboard().instantiateViewController(withIdentifier: "resetPasswordViewController") as! ResetPasswordViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.setupUI()
    }
    
    private func setupUI(){
        self.navigationController?.navigationBar.tintColor = UIColor(named: Color.invertColor)
    }
    
    @IBAction func didTapOnForgotPasswordButton() {
        self.currentTextField?.resignFirstResponder()
        let emailOrPhone = self.usernameTextField.text?.trim()
        var errorTitle : String? = nil
        
        if emailOrPhone == nil || emailOrPhone == "" {
            errorTitle = "Please enter email"
        } else if !Globals.isValidEmail(testStr: emailOrPhone!) {
            errorTitle = "Invalid email"
        }
        
        if errorTitle != nil {
            Globals.showGenericErrorAlert(title:errorTitle, message:"");
            
        }else {
            if !APIRequest.isNetworkAvailable() {
                Globals.shared.showNoInternetConeectionAlert()
                return;
            }
            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.apiRequest = APIRequest.resetPassword(email: emailOrPhone!) { [weak self] (requets, response, error) in
                progressView.hide(animated: true)
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    if let message = responseVO.message() {
                        Globals.showGenericAlert(title:message, message:"");
                    } else {
                        Globals.showGenericAlert(title:"Password reset successfully", message:"");
                    }
                    self?.navigationController?.popViewController(animated: true)
                    
                } else {
                    if let errorMessage = responseVO.errorMessage {
                        Globals.showGenericErrorAlert(title:errorMessage, message:"");
                        
                    } else {
                        Globals.showGenericErrorAlert()
                    }
                }
            }
        }
    }
}

