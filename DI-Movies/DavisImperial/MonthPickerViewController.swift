//
//  PickerViewController.swift
//  Uncle Fitter Mechanic
//
//  Created by Surendra on 7/8/17.
//  Copyright © 2017 Surendra. All rights reserved.
//

import UIKit

class MonthPickerViewController: UIViewController {
    var pickerView : MonthYearPickerView!
    
    override func loadView() {
        pickerView = MonthYearPickerView.init()
        self.view = pickerView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
