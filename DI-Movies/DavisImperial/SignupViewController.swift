//
//  SignupViewController.swift
//  DavisImperial
//
//  Created by Surendra on 6/29/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD
import LocalAuthentication

class SignupViewController: KeyboardReadyBaseViewController {
    

    @IBOutlet weak var firstNameTextField : UITextField!;
    @IBOutlet weak var lastNameTextField : UITextField!;
    @IBOutlet weak var emailTextField : UITextField!;
    @IBOutlet weak var phoneTextField : UITextField!;
    @IBOutlet weak var productionNameTextField : UITextField!;
    @IBOutlet weak var positionTextField : UITextField!;
    @IBOutlet weak var passwordTextField : UITextField!;
    @IBOutlet weak var confirmPasswordTextField : UITextField!;
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var signinButton : UIButton!;
    @IBOutlet weak var stackView : UIStackView!;
    
    
    @IBOutlet weak var formView : UIView!;

    var apiRequest : APIRequest?
    
    static func getInstance() -> SignupViewController {
        return Globals.welcomeStoryboard().instantiateViewController(withIdentifier: kLTSignupVCID) as! SignupViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.scrollViewMain = scrollView
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13, *) {
            self.navigationController?.navigationBar.tintColor = UIColor(named: Color.invertColor)
        }else{
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.tintColor = UIColor(named: Color.invertColor)
            self.navigationController?.navigationBar.barStyle = .default
        }
    }
    
    private func setupUI(){
        self.formView.layer.cornerRadius = 5.0;
        self.formView.clipsToBounds = true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func showDatePicker() {
        let alertController = UIAlertController(title: "Select Date", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let picker = MonthPickerViewController.init()
        alertController.setValue(picker, forKey: "contentViewController")
        
        let somethingAction = UIAlertAction(title: "Done", style: .cancel, handler: {(alert: UIAlertAction!) in
        })
        
        alertController.addAction(somethingAction)
        
        self.present(alertController, animated: true, completion:{})
    }
    
    @IBAction func didTapLoginButton() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func didTapOnSignupButton() {
        
        let firstName = self.firstNameTextField.text?.trim()
        let lastName = self.lastNameTextField.text?.trim()
        let email = self.emailTextField.text?.trim()
        let phone = self.phoneTextField.text?.trim()
        let production = self.productionNameTextField.text?.trim()
        let position = self.positionTextField.text?.trim()
        let password = self.passwordTextField.text?.trim()
        
        var errorTitle : String? = nil
        
        if firstName == nil || firstName == "" {
            errorTitle = diEnterFirstNameMSG
            
        } else if lastName == nil || lastName == "" {
            errorTitle = diEnterLastNameMSG
            
        } else if production == nil || production == "" {
            errorTitle = diEnterProdutionMSG
            
        } else if position == nil || position == "" {
            errorTitle = diEnterPositionMSG
            
        } else if phone == nil || phone == "" {
            errorTitle = diEnterMobileMSG
            
        } else if email == nil || email == "" {
            errorTitle = diEnterEmailMSG
            
        } else if !Globals.isValidEmail(testStr: email!){
            errorTitle = diEnterEmailMSG
            
        } else if password == nil || password == "" {
            errorTitle = diEnterPasswordMSG
        }
        
        if errorTitle != nil {
            self.showErrorAlert(title: errorTitle!, message: "")
            
        }else {
            if !APIRequest.isNetworkAvailable() {
                Globals.shared.showNoInternetConeectionAlert()
                return;
            }
            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.apiRequest = APIRequest.register(firstName: firstName!, lastName:lastName!, email: email!, phone: phone!, password: password!, production: production!, position: position!) { [weak self] (requets, response, error) in
                progressView.hide(animated: true)

                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    if let result = responseVO.response as? [String: Any] {
                        let userObj = result["user"] as! [String : Any]
                        if let user = UserVO.init(json: userObj){
                            let token = result["token"] as! String
                            let tokenId = ""
                            UserManager.shared.setSessionToken(token: token, tokenId:tokenId)
                            UserManager.shared.updateCurrentUser(user: user)
                            KeychainManager.shared.saveAccountDetailsToKeychain(account: email!, password: password!)
                            NotificationCenter.default.post(name: UserManager.userDidLogin, object: nil)
                            PushNotificationManager.registerForPushNotifications()
                        }
                    }
                } else {
                    if let errorMessage = responseVO.errorMessage {
                        self?.showErrorAlert(title: errorMessage, message: "")

                    } else {
                        Globals.showGenericErrorAlert()
                    }
                }
            }
        }
    }
    
    fileprivate func saveAccountDetailsToKeychain(account: String, password: String) {
        print("DATA", account, password)
        guard !account.isEmpty, !password.isEmpty else { return }
        UserDefaults.standard.set(account, forKey: diAccountKey)
        print("DATA", account, password)
        let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: account, accessGroup: KeychainConfiguration.accessGroup)
        print("passwordItem", passwordItem)

        do {
            try passwordItem.savePassword(password)
            print("saved")
        } catch {
            print("Error saving password")
        }
    }
    
    func showErrorAlert(title: String?, message: String?) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Dismiss", style: .cancel, handler: nil))
        alert.showAlert()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }

}
