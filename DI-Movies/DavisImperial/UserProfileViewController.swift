//
//  UserProfileViewController.swift
//  DavisImperial
//
//  Created by Surendra on 7/12/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

let kRowIndexEmail = 0
let kRowIndexPhone = 1
let kRowIndexAddress = 2
let kRowIndexCity = 3

class UserProfileViewController: BaseViewController {
    
    var user : UserVO!
    var detailsApiRequest : APIRequest?
    
    @IBOutlet weak var tableView : UITableView!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        self.navigationItem.leftBarButtonItem = Globals.backBarButton(target: self, action: #selector(onBackBarButtonTap))
        self.tableView.backgroundColor = UIColor(named: Color.pageBackgroundColor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDetails()
        self.reloadUI()
        self.navigationController?.setNavigation(hide: false)
    }
    
    func reloadUI() {
        self.tableView.reloadData()
    }
    static func getInstance() -> UserProfileViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTUserProfileVCID) as! UserProfileViewController
    }
    @objc func onBackBarButtonTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onEditButtonTap() {
        let vc = EditProfileViewController.getInstance()
        vc.user = self.user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func getDetails() {
        self.detailsApiRequest = APIRequest.getSessionUser(result: {[weak self] (request, response, error) in
            let responseVO = ResponseVO.init(response: response, error: error)
            if responseVO.isSuccess {
                if let resultObj = response as? [String : Any] {
                    if let userObj = resultObj["user"] as? [String : Any] {
                        if let user = UserVO.init(json: userObj) {
                            self?.user = user
                            self?.reloadUI()
                            if self?.user.userId == UserManager.shared.currentUser?.userId {
                                UserManager.shared.updateCurrentUser(user: user)
                            }
                            self?.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Edit", style: .plain, target: self, action: #selector(self?.onEditButtonTap))
                        }
                    }
                }
            }
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
}

extension UserProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "cell");
        cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
        cell.detailTextLabel?.minimumScaleFactor = 0.5
        cell.detailTextLabel?.numberOfLines = 2
        cell.backgroundColor = UIColor(named: Color.messageRecieverColor)
        cell.detailTextLabel?.textColor = UIColor(named: Color.darkGrayColor)
        if indexPath.row == kRowIndexEmail {
            cell.textLabel?.text = "Email"
            cell.detailTextLabel?.text = self.user.email
        } else if indexPath.row == kRowIndexPhone {
            cell.textLabel?.text = "Phone"
            cell.detailTextLabel?.text = self.user.phone
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib.init(nibName: "MenuHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as? ProfileHeaderView
        view?.backgroundView?.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        view?.nameLabel.text = self.user.firstName
//        view?.addressLabel?.text = user.addressString()
        return view;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 250;
    }

}
