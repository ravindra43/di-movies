//
//  Booking.swift
//  Davis Imperial
//
//  Created by Surendra on 8/11/17.
//  Copyright © 2017 Surendra. All rights reserved.
//

class BookingVO: NSObject {
    
    var bookingId : String!
    var bookingDisplayId : String = ""
    var bookingDate : Date?
    var proposeDate : Date?
    var proposeTime : String?
    var deliveryDate : Date?
    var deliveryTime : String?

    var isApproved : Bool = false
    var isDateAccept : Bool?
    var isCancelled : Bool = false
    var serviceType: PickupType?
    var deliveryType: PickupType?
    var noRush: Bool? = false

    init(bookingId: String) {
        super.init();
        self.bookingId = bookingId
    }
    
    func showAcceptButton() -> Bool {
        return self.proposeDate != nil && self.isDateAccept == nil
    }
    
    func finalBookingDate() -> Date? {
        return (self.isDateAccept != nil && self.isDateAccept!) && self.proposeDate != nil ? self.proposeDate : self.bookingDate
    }
    
    func statusString() -> String {
        
        var str = "Pending for approval"
        if self.isCancelled {
            str = "Rejected"
        } else if self.isApproved {
            str = "Accepted"
        }
        return str
    }
}

extension BookingVO {
    convenience init?(json: [String: Any]) {
        guard let bookingId = json["_id"] as? String
            else {
                return nil
        }
        
        self.init(bookingId: bookingId);
        if let _isApproved = json["is_approved"] as? Bool {
            self.isApproved = _isApproved
        }
        
        if let _isCanceled = json["is_cancelled"] as? Bool {
            self.isCancelled = _isCanceled
        }
        
        if let _isDateAccept = json["isdate_accept"] as? Bool {
            self.isDateAccept = _isDateAccept
        }
        
        if let _bookingDisplayId = json["booking_id"] as? String {
            self.bookingDisplayId = _bookingDisplayId
        }
        
        if let _bookingDate = json["booking_date"] as? String {
            self.bookingDate = Globals.dateFromString(string: _bookingDate)
        }
        
        if let norush = json["no_rush"] as? Bool {
            self.noRush = norush
        }
        
        if let _praposeDate = json["propose_date"] as? String {
            self.proposeDate = Globals.dateFromString(string: _praposeDate)
            self.serviceType = .schedule
        }else{
            self.serviceType = .asap
        }
        if let _praposeTime = json["propose_time"] as? String {
            self.proposeTime = _praposeTime
        }
        
        if let _deliveryDate = json["preferred_delivery_date"] as? String {
            self.deliveryDate = Globals.dateFromString(string: _deliveryDate)
            self.deliveryType = .schedule
        }else if noRush! {
            self.deliveryType = .noRush
        }else{
            self.deliveryType = .asap
        }
        
        if let _deliverytime = json["preferred_delivery_time"] as? String {
            self.deliveryTime = _deliverytime
        }
    }
}
