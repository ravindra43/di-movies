//
//  NotificationDataSource.swift
//  DavisImperial
//
//  Created by Surendra on 8/26/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

class NotificationDataSource: BaseDataSource {
    
    var items: [NotifVO] = [];
    
    var forActivity = false
    private var apiRequest : APIRequest?
    override func reloadData() {
        super.reloadData()
    }
    
    override func loadItems() {
        if self.status != .loading {
            self.status = .loading;
            weak var weakSelf = self
            self.apiRequest = APIRequest.getNotifications(forActivity:self.forActivity, result: { (request, response, error) in
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    if self.isReloading {
                        self.items = []
                        self.isReloading = false
                    }
                    if let result = response as? [String : Any] {
                        if let resultObj = result["notifications"] as? [[String : Any]] {
                            weakSelf?.status = .idle
                            weakSelf?.allItemsLoaded = true
                            
                            var newItems : [NotifVO] = []
                            for json in resultObj {
                                if self.forActivity {
                                    if let notif = NotifVO.init(activtyJson: json) {
                                        newItems.append(notif)
                                    }
                                } else {
                                    if let notif = NotifVO.init(json: json) {
                                        newItems.append(notif)
                                    }
                                }
                                
                            }
                            
                            weakSelf?.items += newItems
                            
                            weakSelf?.success?()
                        }
                    }
                } else {
                    weakSelf?.status = .failed
                    weakSelf?.failure?()
                    if (response != nil ) {
                        if let responseObject = response as? [String: Any] {
                            if let errorString = responseObject["error"] as? String {
                                if errorString == "Unauthenticated." {
                                    UserManager.logout()
                                }
                            }
                        }
                    }
                }
            })
        }
    }
}

