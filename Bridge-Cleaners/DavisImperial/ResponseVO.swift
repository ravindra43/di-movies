//
//  ResponseVO.swift
//  DavisImperial
//
//  Created by Surendra on 7/6/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class ResponseVO: NSObject {
    
    var isSuccess = false
    var error : Error?
    var errorMessage : String?
    var response : Any?
    
    init(response: Any?, error: Error?) {
        super.init();
        self.response = response
        self.error = error
        if error == nil && response != nil {
            if let responseObj = response as? [String: Any] {
                if let s = responseObj["success"] as? Bool {
                    if s {
                        self.isSuccess = true
                    } else {
                        if let message = responseObj["error"] as? String {
                            if message == "Unauthorized" || message == "Unauthorized Access"{
                                UserManager.logout()
                                return
                            }
                            self.errorMessage = message
                        }
                    }
                }
            }
        }
    }
    
    func resultObj() -> [String: Any]? {
        if let resonseObj = self.response as? [String: Any] {
            if let resultObj = resonseObj["result"] as? [String: Any] {
                return resultObj
            }
        }
        return nil
    }
    
    func resultArray() -> [Any]? {
        if let resonseObj = self.response as? [String: Any] {
            if let resultArray = resonseObj["result"] as? [Any] {
                return resultArray
            }
        }
        return nil
    }
    
    func dataArray() -> [Any]? {
        if let resonseObj = self.response as? [String: Any] {
            if let resultArray = resonseObj["data"] as? [Any] {
                return resultArray
            }
        }
        return nil
    }
    
    func message() -> String? {
        if let resonseObj = self.response as? [String: Any] {
            if let result = resonseObj["msg"] as? String {
                return result
            }
        }
        return nil
    }
}
