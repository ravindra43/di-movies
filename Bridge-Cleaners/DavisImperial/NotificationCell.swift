//
//  NotificationCell.swift
//  DavisImperial
//
//  Created by Surendra on 8/26/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    
    weak var delegate : BookingTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    
    }
    
    override func prepareForReuse() {
        self.titleLabel.text = ""
        self.messageLabel.text = ""
        self.dateLabel.text = ""
    }
    
    func setNotification(notif: NotifVO) {
        
        if notif.createdDate != nil {
            self.dateLabel.text = Globals.formattedDateTime(date: notif.createdDate)
        }
        self.titleLabel.text = notif.title
        self.messageLabel.text = notif.message
    }
}
