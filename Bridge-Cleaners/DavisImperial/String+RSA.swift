//
//  String+RSA.swift
//  DavisImperial
//
//  Created by Surendra on 9/21/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import SwiftyRSA

extension String {
    
    func RSAEncrypt() -> String? {
        do {
            let publicKey = try PublicKey(pemNamed: "rsa-public")
            let clear = try ClearMessage(string: self, using: .utf8)
            let encrypted = try clear.encrypted(with: publicKey, padding: .PKCS1)
            return encrypted.base64String;
            
        } catch {
            print("RSAEncrypt failed")
        }
        return nil
    }
}
