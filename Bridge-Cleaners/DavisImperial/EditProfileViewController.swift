//
//  EditProfileViewController.swift
//  DavisImperial
//
//  Created by Surendra on 8/30/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD

class EditProfileViewController: KeyboardReadyViewController {
    
    @IBOutlet weak var nameTextField : UITextField!;
    @IBOutlet weak var emailTextField : UITextField!;
    @IBOutlet weak var phoneTextField : UITextField!;
    @IBOutlet weak var addressTextField : UITextField!;
    @IBOutlet weak var cityTextField : UITextField!;
    @IBOutlet weak var stackView : UIStackView!;
    @IBOutlet weak var formView : UIView!;
    
    var user : UserVO!
    var apiRequest : APIRequest?
    
    static func getInstance() -> EditProfileViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: "editProfileViewController") as! EditProfileViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.setupUI()
        self.nameTextField.text = self.user.firstName
        self.emailTextField.text = self.user.email
//        self.addressTextField.text = self.user.address
//        self.cityTextField.text = self.user.city
        self.phoneTextField.text = self.user.phone
    }
    
    private func setupUI(){
        self.title = "Edit Profile"
        self.formView.layer.cornerRadius = 5.0;
        self.formView.clipsToBounds = true;
    }
    
    @IBAction func didTapOnUpdateButton() {
        self.currentTextField?.resignFirstResponder()
        
        let name = self.nameTextField.text?.trim()
        let email = self.emailTextField.text?.trim()
        let phone = self.phoneTextField.text?.trim()
        let address = self.addressTextField.text?.trim()
        let city = self.cityTextField.text?.trim()
        var errorTitle : String? = nil
        
        if name == nil || name == "" {
            errorTitle = "Please enter name"
            
        } else if email == nil || email == "" {
            errorTitle = "Please enter email address"
            
        } else if !Globals.isValidEmail(testStr: email!){
            errorTitle = "Invalid email address"
            
        } else if phone == nil || phone == "" {
            errorTitle = "Please enter phone"
            
        } else if address == nil || address == "" {
            errorTitle = "Please enter address"
            
        } else if city == nil || city == "" {
            errorTitle = "Please enter city"
        }
        
        if errorTitle != nil {
            self.showErrorAlert(title: errorTitle!, message: "")
            
        }else {
            if !APIRequest.isNetworkAvailable() {
                Globals.shared.showNoInternetConeectionAlert()
                return;
            }
            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.apiRequest = APIRequest.updateUserProfile(name: name!, email: email!, phone: phone!, address: address!, city: city!) { [weak self] (requets, response, error) in
                progressView.hide(animated: true)
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    Globals.showGenericAlert(title: "Profile updated successfully!", message: "")
                    self?.navigationController?.popViewController(animated: true)
                } else {
                    if let errorMessage = responseVO.errorMessage {
                        self?.showErrorAlert(title: errorMessage, message: "")
                        
                    } else {
                        Globals.showGenericErrorAlert()
                    }
                }
            }
        }
    }
    
    func showErrorAlert(title: String?, message: String?) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Dismiss", style: .cancel, handler: nil))
        alert.showAlert()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
}
