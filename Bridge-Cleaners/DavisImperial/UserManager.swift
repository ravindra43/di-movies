//
//  UserManager.swift
//  DavisImperial
//
//  Created by Surendra on 16/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import Foundation

let kLTDefaultsUserKey = "defaultUser"
let kLTDefaultsShowGTKey = "showGuidedTour"
let kLTDefaultsSessionTokenKey = "sessionToken"
let kLTDefaultsSessionTokenIdKey = "sessionTokenId"

class UserManager: NSObject {
    public static let userDidLogin = Notification.Name("userDidLogin")
    public static let userDidLogout = Notification.Name("userDidLogut")

    var currentUser : UserVO? = nil     
    override init() {
        super.init();
        self.currentUser = getCurrentUser();
    }
    
    static let shared : UserManager = UserManager();
    
    func setSessionToken(token: String?, tokenId: String?)  {
        let defaults = UserDefaults.standard;
        defaults.set(token, forKey: kLTDefaultsSessionTokenKey)
        defaults.set(tokenId, forKey: kLTDefaultsSessionTokenIdKey)
        defaults.synchronize();
    }
    
    func sessionToken() -> String? {
        return UserDefaults.standard.string(forKey: kLTDefaultsSessionTokenKey)
    }
    
    func sessionTokenId() -> String? {
        return UserDefaults.standard.string(forKey: kLTDefaultsSessionTokenIdKey)
    }
    
    func updateCurrentUser(user : UserVO?) {
        let defaults = UserDefaults.standard;
        if (user != nil) {
            let encodedObj = NSKeyedArchiver.archivedData(withRootObject: user!);
            defaults.set(encodedObj, forKey: kLTDefaultsUserKey);
        } else {
            defaults.removeObject(forKey: kLTDefaultsUserKey);
        }
        defaults.synchronize();
        self.currentUser = user;
    }
    
    private func getCurrentUser() -> UserVO? {
        let defaults = UserDefaults.standard;
        let encodedObj = defaults.object(forKey: kLTDefaultsUserKey);
        if (encodedObj != nil) {
            return NSKeyedUnarchiver.unarchiveObject(with: (encodedObj as! Data)) as? UserVO;
        } else {
            return nil;
        }
    }
    
    static func hasValidSession() -> Bool {
        return UserManager.shared.sessionToken() != nil;
    }
    
    static func logout() {
        if PushNotificationManager.shared.apnsToken != nil {
            _ = APIRequest.removeDeviceToken(apnsToken: PushNotificationManager.shared.apnsToken!, result: nil)
        }
        UserManager.shared.setSessionToken(token: nil, tokenId: nil)
        NotificationCenter.default.post(name: UserManager.userDidLogout, object: nil)
    }
    
    func guidedTourShown() -> Bool {
        return UserDefaults.standard.bool(forKey: kLTDefaultsShowGTKey);
    }
    
    func didShowGuidedTour() {
        let defaults = UserDefaults.standard;
        defaults.set(true, forKey: kLTDefaultsShowGTKey);
        defaults.synchronize();
    }
    
}
