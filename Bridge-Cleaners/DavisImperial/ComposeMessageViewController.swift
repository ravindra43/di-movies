//
//  ComposeMessageViewController.swift
//  DavisImperial
//
//  Created by Ravindra Singh on 10/01/18.
//  Copyright © 2018 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD

var isMessageSent: Bool = false

class ComposeMessageViewController: UIViewController, UITextViewDelegate{
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var sendMessageBtn: UFButton!
    
    var composeMessageRequest : APIRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(onCancel))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        messageTextView.resignFirstResponder()
    }
    
    private func setupUI(){
        
    }
    
    @objc func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onSendButtonClick(_ sender: Any) {
        messageTextView.resignFirstResponder()

        let messageText = self.messageTextView.text?.trim()
        var errorTitle: String? = nil
        if messageText == nil || messageText == "" {
            errorTitle = "Please enter message"
        }
        if errorTitle != nil {
            Globals.showGenericErrorAlert(title: errorTitle, message: "")
        } else {
            if !APIRequest.isNetworkAvailable() {
                Globals.shared.showNoInternetConeectionAlert()
                return;
            }
            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
            
            self.composeMessageRequest = APIRequest.sendMessage(message: messageText!) {(requets, response, error) in
                progressView.hide(animated: true)
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    isMessageSent = true;
                    self.dismiss(animated: true, completion: nil)
                
                } else {
                    if let errorMessage = responseVO.errorMessage {
                        Globals.showGenericErrorAlert(title: errorMessage, message: "")
                    } else {
                        Globals.showGenericErrorAlert()
                    }
                }
            }
        }
    }
    
    func showErrorAlert(title: String?, message: String?) {
        Globals.showGenericErrorAlert(title: title, message: message)
    }
}
