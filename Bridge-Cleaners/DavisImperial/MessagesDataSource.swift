//
//  MessagesDataSource.swift
//  DavisImperial
//
//  Created by Ravindra Singh on 09/01/18.
//  Copyright © 2018 Encoresky. All rights reserved.
//

class MessagesDataSource: BaseDataSource {
    
    var items: [MessagesVO] = [];
    
    var forActivity = false
    private var apiRequest : APIRequest?
    override func reloadData() {
        super.reloadData()
    }
    
    override func loadItems() {
        if self.status != .loading {
            self.status = .loading;
            let limit = 1000000
            weak var weakSelf = self
            self.apiRequest = APIRequest.getMessages(pageNo: Int(page), limit: limit, result: { (request, response, error) in
                
                let responseVO = ResponseVO.init(response: response, error: error)
                if responseVO.isSuccess {
                    if self.isReloading {
                        self.items = []
                        self.isReloading = false
                    }
                    if let result = responseVO.response as? [String : Any] {

                        if result["chat"] is NSNull{
                            weakSelf?.allItemsLoaded = true
                            weakSelf?.items = []
                            weakSelf?.success?()
                            return
                        }
                        if let chat = result["chat"] as? [String : Any] {
                            if chat.count == 0 {
                                weakSelf?.items = []
                                weakSelf?.success?()
                                return
                            }
                            if let resultObj = chat["messages"] as? [[String : Any]] {
                                print("MESSAGES",resultObj)
                                weakSelf?.status = .idle
                                weakSelf?.allItemsLoaded = resultObj.count < limit
                                var newItems : [MessagesVO] = []
                                for json in resultObj {
                                    if let msg = MessagesVO.init(json: json) {
                                        newItems.append(msg)
                                    }
                                }
                                weakSelf?.items = newItems
                                weakSelf?.success?()

                            }else{
                                weakSelf?.items = []
                                weakSelf?.success?()
                                weakSelf?.allItemsLoaded = true

                            }
                        }else{
                            weakSelf?.items = []
                            weakSelf?.success?()
                            weakSelf?.allItemsLoaded = true
                        }
                    }
                } else {
                    print("response")
                    weakSelf?.status = .failed
                    weakSelf?.failure?()
                    if (response != nil ) {
                        if let responseObject = response as? [String: Any] {
                            if let errorString = responseObject["error"] as? String {
                                Globals.showGenericErrorAlert(title: errorString, message: "")
                                if errorString == "Unauthenticated." {
                                    UserManager.logout()
                                }
                            }
                        }
                    }
                }
            })
        }
    }
}
