//
//  KeyboardReadyTableViewController.swift
//  DavisImperial
//
//  Created by Surendra on 9/21/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

import UIKit

class KeyboardReadyBaseViewController: UIViewController, UITextFieldDelegate {
    
    var currentTextField : UITextField?
    var scrollViewMain: UIScrollView?
    var isKeyboardmovement = true
    private var m_tapGestureReconizer : UITapGestureRecognizer?
    
    public func containerView() -> UIView {
        return self.view
    }
    override func viewDidLoad() {
        setNotificationKeyboard()
    }
    
    private func addGestureRecognizer() {
        self.m_tapGestureReconizer = UITapGestureRecognizer.init(target: self, action: #selector(handleGestureReconizer(_:)));
        self.m_tapGestureReconizer?.numberOfTapsRequired = 1;
        self.containerView().addGestureRecognizer(self.m_tapGestureReconizer!)
    }
    // notification for keyboard appearance //
    func setNotificationKeyboard ()  {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UITextView.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UITextView.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification) {
        if let scrollview = self.scrollViewMain {
            if isKeyboardmovement{
                let info = notification.userInfo!
                let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
                let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height+40, right: 0.0)
                
                scrollview.contentInset = contentInsets
                scrollview.scrollIndicatorInsets = contentInsets
                var aRect : CGRect = self.view.frame
                aRect.size.height -= keyboardSize!.height
                if let activeField = self.currentTextField
                {
                    if (!aRect.contains(activeField.frame.origin))
                    {
                        scrollview.scrollRectToVisible(activeField.frame, animated: true)
                    }
                }
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
            self.scrollViewMain?.contentInset.bottom = 0
        }, completion: nil)
//                let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,0.0, 0.0)
//                self.scrollViewMain!.contentInset = contentInsets
//                self.scrollViewMain!.scrollIndicatorInsets = contentInsets
////                self.view.endEditing(true)
    }
    @objc private func handleGestureReconizer(_ sender : UITapGestureRecognizer){
        self.currentTextField?.resignFirstResponder()
    }
    
    private func removeGestureRecognizer() {
        if self.m_tapGestureReconizer != nil {
            self.containerView().removeGestureRecognizer(self.m_tapGestureReconizer!);
            self.m_tapGestureReconizer = nil;
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
        self.addGestureRecognizer()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.removeGestureRecognizer()
        self.currentTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false;
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
