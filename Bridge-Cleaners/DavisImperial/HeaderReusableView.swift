//
//  HeaderReusableView.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 31/01/20.
//  Copyright © 2020 Encoresky. All rights reserved.
//

import UIKit

class HeaderReusableView: UICollectionReusableView {
        
    @IBOutlet weak var headerLabel: UILabel!
}
