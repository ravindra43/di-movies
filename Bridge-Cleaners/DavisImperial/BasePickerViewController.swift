//
//  BasePickerViewController.swift
//  DavisImperial
//
//  Created by Surendra on 7/22/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

@objc protocol BasePickerViewControllerDelegate {
    func numberOfItems(picker : BasePickerViewController, section: Int) -> Int
    func configureCellAtIndex(picker : BasePickerViewController, indexPath: IndexPath, cell: UITableViewCell)
    func pickerDidReceiveSearchText(picker : BasePickerViewController, text: String)

    func pickerDidSelect(picker : BasePickerViewController, indexPath: IndexPath)
    func pickerDidCancel(picker : BasePickerViewController)
    
    func cellAtIndex(picker : BasePickerViewController, indexPath: IndexPath) -> UITableViewCell?
}

class BasePickerViewController: UIViewController {

    weak var pickerDelegate : BasePickerViewControllerDelegate? = nil;
    
    var showCancel = true
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var searchBar : UISearchBar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()

        if self.showCancel {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(onCancelButtonTap))
        }
        self.view.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        self.tableView.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        self.tableView.tableFooterView = UIView.init()
        if self.searchBar != nil {
            self.searchBar?.backgroundImage = UIImage.init()
            self.searchBar?.backgroundColor = UIColor(named: Color.pageBackgroundColor)
            //self.searchBar?.setBackgroundImage(UIImage.init(), for: .any, barMetrics: .default)
        }
        // Do any additional setup after loading the view.
    }
    
    func reloadData() {
        self.tableView.reloadData()
    }
    
    func onBackBarButtonTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onCancelButtonTap() {
        self.pickerDelegate?.pickerDidCancel(picker: self)
    }
}

extension BasePickerViewController : UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.pickerDelegate?.pickerDidReceiveSearchText(picker: self, text: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}


extension BasePickerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pickerDelegate == nil ? 0 : (self.pickerDelegate?.numberOfItems(picker: self, section: section))!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = self.pickerDelegate?.cellAtIndex(picker: self, indexPath: indexPath)
        if cell == nil {
            cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "cell")
            cell?.textLabel?.adjustsFontSizeToFitWidth = true
            cell?.textLabel?.minimumScaleFactor = 0.5
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
            cell?.textLabel?.textColor = UIColor(named: Color.textLabelColor)
            self.pickerDelegate?.configureCellAtIndex(picker: self, indexPath: indexPath, cell: cell!)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pickerDelegate?.pickerDidSelect(picker: self, indexPath: indexPath)
    }
}

