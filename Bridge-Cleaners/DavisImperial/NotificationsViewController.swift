//
//  NotificationsViewController.swift
//  DavisImperial
//
//  Created by Surendra on 8/26/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD
class NotificationsViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var noItemsLabel : UILabel!
    
    var refreshControl = UIRefreshControl.init()
    var progressHud : MBProgressHUD?
    
    var needsToRefresh = false
    var forActivity = false

    fileprivate var dataSource : NotificationDataSource!
    
    static func getInstance() -> NotificationsViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: "notificationsViewController") as! NotificationsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.title = self.forActivity ? "Activity Notifications" : "Notifications"
        
        self.tableView.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.rowHeight = UITableView.automaticDimension
        self.refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tableView.addSubview(self.refreshControl)
        
        self.dataSource = NotificationDataSource.init(success: { [weak self] () -> Void in self?.itemsLoaded()}, failure: { [weak self] () -> Void in self?.itemLoadFailed()})
        self.dataSource.forActivity = self.forActivity
        self.showLoading()
        self.dataSource.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func onBackBarButtonTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func onBookingStatusChangedNotification() {
        self.needsToRefresh = true
    }
    
    @objc func onRefresh(){
        self.noItemsLabel.isHidden = true
        self.dataSource.reloadData()
    }
    
    private func itemsLoaded()  {
        self.refreshControl.endRefreshing()
        self.hideLoading()
        self.tableView.reloadData()
        self.needsToRefresh = false
        self.noItemsLabel.text = "No items found."
        self.noItemsLabel.isHidden = self.dataSource.items.count != 0
    }
    
    private func itemLoadFailed()  {
        self.refreshControl.endRefreshing()
        self.hideLoading()
         self.dataSource.items = []
        self.tableView.reloadData()
        self.noItemsLabel.text = "Something went wrong. Couldn't load items."
        self.noItemsLabel.isHidden = false;
    }
    
    fileprivate func showLoading(){
        self.progressHud = MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    fileprivate func hideLoading(){
        self.progressHud?.hide(animated: true)
    }
}

extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSource.status == .loading ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifCell", for: indexPath) as! NotificationCell
        cell.setNotification(notif: self.dataSource.items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.shouldLoadMoreItems() {
            if self.dataSource.status != .failed && self.dataSource.status != .loading && !self.dataSource.allItemsLoaded {
                self.showLoading()
                self.dataSource.loadNextPage()
            }
        }
    }
}

