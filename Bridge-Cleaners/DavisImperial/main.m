//
//  main.m
//  DavisImperial
//
//  Created by Surendra on 07/04/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DavisImperial-Swift.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
