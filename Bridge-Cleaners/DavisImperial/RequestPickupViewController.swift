//
//  RequestPickupViewController.swift
//  DavisImperial
//
//  Created by Surendra on 7/22/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import MBProgressHUD

@objc protocol RequestPickupViewControllerDelegate {
    func onBookingDone(requestPickupVC : RequestPickupViewController);
}

class RequestPickupViewController: BaseViewController {
    
    @IBOutlet weak var slotCollectionView: UICollectionView!
    @IBOutlet var datePicker : UIDatePicker!
    var slotArray = ["12am - 2am", "2am - 4am", "4am - 6am", "6am - 8am", "8am - 10am", "10am - 12pm", "12pm - 2pm", "2pm - 4pm", "4pm - 6pm", "6pm - 8pm", "8pm - 10pm", "10pm - 12am"]
    weak var delegate : RequestPickupViewControllerDelegate? = nil;
    var isDelivery: Bool = false
    var selectedSlot:String? = nil
    
    static func getInstance() -> RequestPickupViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: "requestPickupViewController") as! RequestPickupViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
        self.title = "Pick a Date"
        slotCollectionView.delegate = self;
        slotCollectionView.dataSource = self;
        slotCollectionView.reloadData()
        
        self.datePicker.minimumDate = Date()
        self.datePicker.datePickerMode = .date
    }
    
    @IBAction func onNextButtonTap(button: UIButton) {
        if selectedSlot != nil {
            if isDelivery {
                let dateString = Globals.formattedForLocalDateTime(date: datePicker.date)
                ServiceDS.shared.deliveryDate = dateString
                confirmBooking()
            }else{
                if #available(iOS 9.3, *) {
                    let vc = LocationOptionViewController.getInstance()
                    let dateString = Globals.formattedForLocalDateTime(date: datePicker.date)
                    ServiceDS.shared.scheduleDate = dateString
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            Globals.showGenericAlert(title: "Please select any time slot", message: nil)
        }
    }
    
    func confirmBooking(){
        if !APIRequest.isNetworkAvailable() {
            Globals.shared.showNoInternetConeectionAlert()
            return;
        }
        let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
        _ = APIRequest.bookServise(result: { (request, response, error) in
            progressView.hide(animated: true)
            let responseVO = ResponseVO.init(response: response, error: error)
            if responseVO.isSuccess{
                Globals.showGenericAlert(title: "Service booked successfully", message: "", complition: {
                    NotificationCenter.default.post(name: UserManager.userDidLogin, object: nil)
                    ServiceDS.shared.clearService()
                })
            }else{
                Globals.showGenericAlert(title: responseVO.errorMessage, message: "")
            }
        })
    }
    deinit {
        if isDelivery {
            ServiceDS.shared.deliveryDate = nil
            ServiceDS.shared.deliveryTime = nil
        }else{
            ServiceDS.shared.scheduleDate = nil
            ServiceDS.shared.scheduleTime = nil
        }
    }
}

extension RequestPickupViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 2
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slotArray.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SlotCollectionViewCell = slotCollectionView.dequeueReusableCell(withReuseIdentifier: "SlotCollectionViewCell", for: indexPath) as! SlotCollectionViewCell;
        cell.config(data: slotArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.slotCollectionView.frame.width/3 - 10, height: 44);
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = slotArray.firstIndex(where: {$0 == selectedSlot}) {
            if let cell:SlotCollectionViewCell = slotCollectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? SlotCollectionViewCell{
                cell.backgroundColor = .clear;
                cell.slotButton.setTitleColor(UIColor(named: Color.textLabelColor), for: .normal)
            }
        }
        selectedSlot = slotArray[indexPath.row]
        if let cell:SlotCollectionViewCell = slotCollectionView.cellForItem(at: indexPath) as? SlotCollectionViewCell {
            cell.backgroundColor = UIColor(named: Color.buttonBackgroundColor)
            cell.slotButton.setTitleColor(UIColor(named: Color.buttonTextColor), for: .normal)
        }
        if isDelivery {
            ServiceDS.shared.deliveryTime = selectedSlot
        }else{
            ServiceDS.shared.scheduleTime = selectedSlot
        }
    }
    
}
