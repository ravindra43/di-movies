//
//  KeyboardReadyViewController.swift
//  DavisImperial
//
//  Created by Surendra on 6/28/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class KeyboardReadyViewController: UIViewController, UITextFieldDelegate {

    private var animatedDistance : CGFloat = 0
    private var keyboardFrame : CGRect?
    var currentTextField : UITextField?
    private var m_tapGestureReconizer : UITapGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow(_:)), name: UITextView.keyboardWillShowNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func containerView() -> UIView {
        return self.view
    }
    
    private func addGestureRecognizer() {
        self.m_tapGestureReconizer = UITapGestureRecognizer.init(target: self, action: #selector(handleGestureReconizer(_:)));
        self.m_tapGestureReconizer?.numberOfTapsRequired = 1;
        self.containerView().addGestureRecognizer(self.m_tapGestureReconizer!)
        
    }
    
    @objc private func handleGestureReconizer(_ sender : UITapGestureRecognizer){
        self.currentTextField?.resignFirstResponder()
    }
    
    private func removeGestureRecognizer() {
        if self.m_tapGestureReconizer != nil {
            self.containerView().removeGestureRecognizer(self.m_tapGestureReconizer!);
            self.m_tapGestureReconizer = nil;
        }
    }
    
    @objc private func keyboardWillShow (_ sender : Notification){
        if let dict = sender.userInfo as NSDictionary? as! [String:Any]? {
            let keyboardFrameValue : NSValue = dict[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            self.keyboardFrame = keyboardFrameValue.cgRectValue;
        }
    }

    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
        self.addGestureRecognizer()
        self.perform(#selector(onTextFieldDidBeginEditing(_:)), with: textField, afterDelay: 0.001)
    }
    
    @objc private func onTextFieldDidBeginEditing(_ textField : UITextField) {
        if (self.keyboardFrame == nil || (self.keyboardFrame?.isNull)! || (self.keyboardFrame?.isEmpty)!) {
            let keyboardHeight : Double = 216
            //let windowHeight : CGFloat = (self.containerView().window?.bounds.size.height)!
            self.keyboardFrame = CGRect.init(x: 0, y: 350, width: 100, height: keyboardHeight)
        }
        
        let textFieldRect : CGRect = (self.containerView().window?.convert(textField.bounds, from: textField))!
        let offset : CGFloat = textFieldRect.maxY - (self.keyboardFrame?.minY)! + 70
        if offset > 0 {
            self.animatedDistance = offset
        }
        
        var viewFrame : CGRect = self.containerView().frame
        viewFrame.origin.y -= self.animatedDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(0.3)
        self.containerView().frame = viewFrame
        
        UIView.commitAnimations()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var viewFrame : CGRect = self.containerView().frame
        if self.animatedDistance > 0 {
            viewFrame.origin.y += self.animatedDistance;
            self.animatedDistance = 0;
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(0.3)
            self.containerView().frame = viewFrame
            
            UIView.commitAnimations()
        }
        self.removeGestureRecognizer()
        self.currentTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false;
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
