//
//  String+Add.swift
//  DavisImperial
//
//  Created by Surendra on 7/1/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import Foundation

extension String {
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func urlEncode() -> String {
        let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!
    }
    
}
