//
//  SlotCollectionViewCell.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 31/01/20.
//  Copyright © 2020 Encoresky. All rights reserved.
//

import UIKit

class SlotCollectionViewCell: UICollectionViewCell {
        
    @IBOutlet weak var slotButton: UIButton!
    func config(data:String) {
//        self.contentView.backgroundColor = .white
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = 22
        self.slotButton.setTitle(data, for: .normal)
    }
    func layout(){
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.8
        self.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layoutMargins = .zero // remove table cell separator margin
        self.contentView.layoutMargins.top = 20
        self.contentView.layoutMargins.bottom = 20

    }
}
