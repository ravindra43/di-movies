//
//  AppDelegate.swift
//  DavisImperial
//
//  Created by Surendra on 18/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Alamofire
import UserNotifications
import AVFoundation
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?;
    var sideMenu : SlideMenuController?;
    let reachabilityManager = NetworkReachabilityManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        self.customizeAppearance();
        NotificationCenter.default.addObserver(self, selector:#selector(AppDelegate.userDidLogin), name:UserManager.userDidLogin, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(AppDelegate.userDidLogout), name:UserManager.userDidLogout, object: nil)
        PushNotificationManager.registerForPushNotifications()
        application.applicationIconBadgeNumber = 0
        self.window = UIWindow.init(frame: UIScreen.main.bounds);
        self.window?.backgroundColor = UIColor(named: Color.pageBackgroundColor);
        if UserManager.shared.guidedTourShown() {
            if (!UserManager.hasValidSession()) {
                self.window?.rootViewController = self.loginNavigationViewController();
                
            } else {
                PushNotificationManager.registerForPushNotifications()
                self.sideMenu = self.slideMenuController();
                self.window?.rootViewController = self.sideMenu;
                self.window?.makeKeyAndVisible();
            }
        } else {
            let guideTourVC = Globals.welcomeStoryboard().instantiateViewController(withIdentifier: kLTGuidedTourVCID) as! GuidedTourViewController
            guideTourVC.delegate = self
            self.window?.rootViewController = guideTourVC
        }
        DispatchQueue.main.async {
            AVCaptureDevice.requestAccess(for: .video) { (access) in print("video access", access) }
        }
        PushNotificationManager.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        if UserManager.hasValidSession() {
            let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
            if !isRegisteredForRemoteNotifications {
                notificationEnable()
            }
        }
        return true;
    }
    func notificationEnable() {
        Globals.showConfirmationAlert(title: "To continue receiving notifications in the app, please update the settings.", message: nil, cancelButtonTitle: "Cancel", actionButtonTitle: "Update") {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl as URL)
                }
            }
        }
    }
    func reachabilityDidChange() {
//        reachabilityManager?.startListening()
//        reachabilityManager?.listener = { _ in
//            if let isNetworkReachable = self.reachabilityManager?.isReachable,
//                isNetworkReachable == true {
//                if UserManager.hasValidSession() {
//                    SocketIOManager.shared.establishConnection()
//                }
//            } else {
//                SocketIOManager.shared.closeConnection()
//            }
//        }
        Alamofire.NetworkReachabilityManager()?.startListening(onUpdatePerforming: {(status) in
            switch status{
                case .unknown:
                    print("Unknown network reachability")
                    break
                case .notReachable:
                    print("Not reachable network")
                    SocketIOManager.shared.closeConnection()
                    break
                case .reachable(.cellular),.reachable(.ethernetOrWiFi):
                    print("Reachable network")
                    if UserManager.hasValidSession() {
                        SocketIOManager.shared.establishConnection()
                    }
                    break
            }
        })
//        Alamofire.NetworkReachabilityManager()?.startListening(onUpdatePerforming: { _ in
//            if let isNetworkReachable = self.reachabilityManager?.isReachable,
//            isNetworkReachable == true {
//                if UserManager.hasValidSession() {
//                    SocketIOManager.shared.establishConnection()
//                }
//            } else {
//                SocketIOManager.shared.closeConnection()
//            }
//        })
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       PushNotificationManager.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushNotificationManager.application(application, didFailToRegisterForRemoteNotificationsWithError: error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if (UserManager.hasValidSession()) {
            if let aps = userInfo["aps"] as? [AnyHashable : Any] {
                if let info: String = aps["category"] as? String  {
                    if (info == "booking") {
                        let vc = BookingsViewController.getInstance()
                        vc.showBack = true
                        let navigationController = UINavigationController.init(rootViewController: vc);
                        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0)]
                        self.window?.rootViewController = navigationController
                    } else if (info == "message") {
                        let vc = MessagesViewController.getInstance()
                        vc.showBack = true
                        let navigationController = UINavigationController.init(rootViewController: vc);
                        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0)]
                        self.window?.rootViewController = navigationController
                    } else {
                        PushNotificationManager.application(application, didReceiveRemoteNotification: userInfo)
                    }
                } else {
                    PushNotificationManager.application(application, didReceiveRemoteNotification: userInfo)
                }
            }
        }
    }
    
    func slideMenuController() -> SlideMenuController {
        reachabilityDidChange()
        let centerViewController = HomeViewController.getInstance();
        let navigationController = UINavigationController.init(rootViewController: centerViewController);
//        Globals.hideBottomLine(navBar: navigationController.navigationBar)
        let sideMenu = Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTMainMenuVCID);
        let slideMenuController = SlideMenuController.init(mainViewController: navigationController, leftMenuViewController: sideMenu);
        SlideMenuOptions.animationDuration = kLTMainMenuAnimationDuration;
        SlideMenuOptions.panGesturesEnabled = false;
        SlideMenuOptions.simultaneousGestureRecognizers = false;
        SlideMenuOptions.panFromBezel = false;
        SlideMenuOptions.contentViewScale = 1.0
        SlideMenuOptions.tapGesturesEnabled = true;
        SlideMenuOptions.opacityViewBackgroundColor = UIColor.clear;
        
        return slideMenuController
    }
    
    func loginNavigationViewController() -> UINavigationController {
        let vc = Globals.welcomeStoryboard().instantiateViewController(withIdentifier: kLTLoginVCID)
        let navigationController = UINavigationController.init(rootViewController: vc);
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.darkGray, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0)]
        return navigationController
    }
    
    func signupNavigationViewController() -> UINavigationController {
        let vc = Globals.welcomeStoryboard().instantiateViewController(withIdentifier: kLTSignupVCID)
        let navigationController = UINavigationController.init(rootViewController: vc);
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.darkGray, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0)]
        return navigationController
    }
    
    func customizeAppearance() {
        if #available(iOS 13, *) {}else{
            UINavigationBar.appearance().barTintColor = UIColor(named: Color.pageBackgroundColor);
            UISearchBar.appearance().barTintColor = UIColor(named: Color.pageBackgroundColor);
            UISearchBar.appearance().tintColor = UIColor(named: Color.invertColor);
            UINavigationBar.appearance().tintColor = UIColor(named: Color.whiteColor);
            UINavigationBar.appearance().barStyle = .black
            UINavigationBar.appearance().isTranslucent = false;
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0)]
        }
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        if UserManager.hasValidSession() {
            SocketIOManager.shared.establishConnection()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if UserManager.hasValidSession() {
            SocketIOManager.shared.closeConnection()
        }
    }
}

// Notification Handlers and Guided tour delegate
extension AppDelegate : GuidedTourViewControllerDelegate {
    
    @objc func userDidLogin() {
        self.sideMenu = self.slideMenuController();
        self.window?.rootViewController = self.sideMenu
        self.window?.makeKeyAndVisible();
        UIView.transition(with: self.window!, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion: nil)
    }
    
    @objc func userDidLogout() {
        let loginVC = self.loginNavigationViewController()
        self.window?.rootViewController = loginVC
        self.window?.makeKeyAndVisible();
        UIView.transition(with: self.window!, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion: nil)
    }
    
    func onGuidedTourVisitCompleted(guidedTour: GuidedTourViewController, forSignin: Bool) {
        let navVC = forSignin ? self.loginNavigationViewController() : self.signupNavigationViewController()
        self.window?.rootViewController = navVC
        self.window?.makeKeyAndVisible();
        UIView.transition(with: self.window!, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion: nil)
        UserManager.shared.didShowGuidedTour()
    }
}
