//
//  LocationViewController.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 09/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AddressBookUI
import MBProgressHUD

@available(iOS 9.3, *)
class LocationViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var addressView: DIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var placeListHeight: NSLayoutConstraint!
    @IBOutlet weak var placeTableView: UITableView!
    var isSendLocation = false
    let locationManager =  CLLocationManager()
    var searchCompleter = MKLocalSearchCompleter()
    var placeList = [MKLocalSearchCompletion]()
    var newPin = MKPointAnnotation()

    let searchController = UISearchController(searchResultsController: nil)
    var currentLat: Double? = nil
    var currentLong: Double? = nil
    var currentAddressTitle: String? = nil
    var currentAddressSubTitle: String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pick Up Location"
        locationManager.delegate = self
        searchCompleter.delegate = self
        placeListHeight.constant = 0
        
        //MARK: MAPVIEW SETUP
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        mapView.showsUserLocation = true
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        let tapOnView = UITapGestureRecognizer(target: self, action: #selector(keyboardHide))
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(mapLongPress(_:)))
        tapOnView.numberOfTapsRequired = 1
        tapOnView.cancelsTouchesInView = false
        mapView.addGestureRecognizer(tapOnView)
        view.addGestureRecognizer(tapOnView)
        mapView.addGestureRecognizer(longPress)

        //MARK: TABLEVIEW SETUP//
        self.placeTableView.allowsSelection = true
        self.placeTableView.contentInset.bottom = 30
        
        //MARK: SEARCHCONTROLLER SETUP
        self.searchController.searchResultsUpdater = self as UISearchResultsUpdating
        searchController.searchResultsUpdater = self
        searchController.searchBar.searchBarStyle = .default
        searchController.searchBar.barStyle       = .default
        let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField
        textfield!.backgroundColor = UIColor(named: Color.pageBackgroundColor)
        textfield?.textColor = UIColor(named: Color.textLabelColor)
         searchController.obscuresBackgroundDuringPresentation = false
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController   = searchController
        }
        searchController.searchBar.placeholder = "Search or Enter Location"
        searchController.searchBar.returnKeyType = .done
        definesPresentationContext = true
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
    }
    @objc func keyboardHide(){
        self.searchController.searchBar.resignFirstResponder()
    }
    
    @objc func mapLongPress(_ recognizer: UIGestureRecognizer) {
        mapView.removeAnnotation(newPin)
        let touchedAt = recognizer.location(in: self.mapView) // adds the location on the view it was pressed
        let touchedAtCoordinate : CLLocationCoordinate2D = mapView.convert(touchedAt, toCoordinateFrom: self.mapView)
        newPin.coordinate = touchedAtCoordinate
        mapView.addAnnotation(newPin)
        mapView.deselectAnnotation(mapView.userLocation, animated: true)

        let location = CLLocation(latitude: touchedAtCoordinate.latitude, longitude: touchedAtCoordinate.longitude)
        setGeoLocation(location: location, isCurrent: false)
    }
    
    func setGeoLocation(location: CLLocation, isCurrent: Bool) {
        ServiceDS.shared.location = (location.coordinate.latitude, location.coordinate.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { (placeMarks, error) in
            if error != nil {
                print("Location Error!")
                return
            }
            
            if let pm = placeMarks?.first {
                if let place = pm.addressDictionary!["SubLocality"]{
                    self.placeName.text = place as? String
                    self.newPin.title = place as? String
                    if isCurrent{ self.currentAddressTitle = place as? String }
                }
                let addressString = ABCreateStringWithAddressDictionary(pm.addressDictionary!, true)
                self.addressLabel.text = addressString.replacingOccurrences(of: "\n", with: ", ", options: .literal, range: nil)
                if isCurrent{ self.currentAddressSubTitle = addressString}
                let place = self.placeName.text!
                let address = self.addressLabel.text!
                ServiceDS.shared.address = place + address
                ServiceDS.shared.location = (pm.location?.coordinate.latitude, pm.location?.coordinate.longitude)
            } else {
                print("error with data")
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        mapView.removeAnnotation(newPin)

        let location = locations.last! as CLLocation

        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
        mapView.setRegion(region, animated: true)
        setGeoLocation(location: location, isCurrent: true)
        currentLat = Double(location.coordinate.latitude)
        currentLong = Double(location.coordinate.longitude)
        locationManager.stopUpdatingLocation()
    }
    
    static func getInstance() -> LocationViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTlocationVCID) as! LocationViewController
    }
    
    @IBAction func onPressNextButton(_ sender: Any) {
        if ServiceDS.shared.address != nil {
            let vc = BagSelectionViewController.getInstance()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            Globals.showGenericErrorAlert(title: "Please select location", message: "")
        }
    }
    
//    func sendLoaction() {
//        if ServiceDS.shared.address != nil {
//            let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
//            progressView.show(animated: true)
//            APIRequest.sendLocation { (request, response, error) in
//                progressView.hide(animated: true);
//                let responseVO = ResponseVO(response: response, error: error)
//                if responseVO.isSuccess {
//                    if let response = response as? [String: Any]{
//                        Globals.showGenericAlert(title: response["message"] as? String, message: nil) {
//                            self.navigationController?.popViewController(animated: true)
//                        }
//                    }
//                }
//            }
//        }
//    }
    deinit {
        ServiceDS.shared.location = (nil, nil);
        ServiceDS.shared.address = nil;
    }
    
}

@available(iOS 9.3, *)
extension LocationViewController: MKLocalSearchCompleterDelegate{
    //MARK:SEARCH RESULTS RECIEVED
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        let searchResults = completer.results
        placeList = searchResults
        if searchResults.count > 0 && self.placeListHeight.constant == 0  {
            UIView.animate(withDuration: 0.3,
                           delay: 0.1,
                           options: UIView.AnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.placeListHeight.constant = self.mapView.frame.height
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
        placeTableView.reloadData()
    }
}

@available(iOS 9.3, *)
extension LocationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        if placeList.count <= indexPath.row  {
            return cell
        }
        cell.detailTextLabel?.font = UIFont(name: "System", size: 14)
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        cell.textLabel?.text = placeList[indexPath.row].title //+ "\n" + placeList[indexPath.row].subtitle
        cell.detailTextLabel?.text = placeList[indexPath.row].subtitle
        cell.textLabel?.numberOfLines = 3
        cell.contentView.backgroundColor = UIColor(named: Color.messageRecieverColor)
        cell.backgroundColor = .clear
        return cell
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchController.searchBar.resignFirstResponder()
    }
    //MARK:LOCATION SELECTED
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        placeTableView.deselectRow(at: indexPath, animated: true)
        let selectedLocation = placeList[indexPath.row]
        
        ServiceDS.shared.location = (nil, nil)
        ServiceDS.shared.address  = selectedLocation.title + selectedLocation.subtitle
        
        searchController.searchBar.resignFirstResponder()
        
        if placeList[indexPath.row].title == "My Location" || placeList[indexPath.row].title == "Find My location" || placeList[indexPath.row].title == "Current Location"{
            locationManager.startUpdatingLocation()
            return
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.placeName.text = selectedLocation.title
                self.addressLabel.text = selectedLocation.subtitle
                self.placeList = []
            }
            
            UIView.animate(withDuration: 0.3) {
                self.placeListHeight.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation === mapView.userLocation {
            return nil
        }
        let annnotaionView = CustomAnnotationView()
        annnotaionView.selectedLabel.text = annotation.title!
        return annnotaionView
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation {
            print("Your annotation title: \(String(describing: annotation.title))");
            if annotation.title == "My Location" || annotation.title == ""{
                locationManager.startUpdatingLocation()
                mapView.removeAnnotation(newPin)
                return
            }else{
                mapView.deselectAnnotation(mapView.userLocation, animated: true)
            }
        }
    }
}

class CustomAnnotationView : MKPinAnnotationView
{
    let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 140, height: 40))
    var selectedLabel = UILabel()
    var lat           = Double()
    var long:Double?
    var mainAnnotation: MKAnnotation?
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier);
        titleView.backgroundColor = .clear
        titleView.layer.zPosition = -1
        selectedLabel.frame = CGRect(x: 2, y: 2, width: titleView.frame.width - 4, height: titleView.frame.height - 4)
        
        selectedLabel.textAlignment = .center
        selectedLabel.font = UIFont.init(name: "Avenir Next LT Pro", size: 12)
        selectedLabel.backgroundColor = UIColor(named: Color.messageRecieverColor)
        selectedLabel.layer.borderColor = UIColor(named: Color.invertColor)?.cgColor
        selectedLabel.textColor = UIColor(named: Color.invertColor)
        selectedLabel.layer.borderWidth = 2
        selectedLabel.layer.cornerRadius = selectedLabel.frame.height / 2
        selectedLabel.layer.masksToBounds = true
        selectedLabel.layer.borderWidth   = 0.3
        selectedLabel.center.x =  -1.5 * self.frame.size.width;
        selectedLabel.center.y = selectedLabel.frame.height;
        selectedLabel.numberOfLines = 2
        titleView.addSubview(selectedLabel)
        self.addSubview(titleView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTapAnnotaion))
        self.titleView.addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func onTapAnnotaion(){
        self.setSelected(true, animated: true)
    }

}

@available(iOS 9.3, *)
extension LocationViewController: UISearchResultsUpdating, UISearchBarDelegate{
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text
        searchCompleter.queryFragment = searchText!
        
        if searchText == "" {
            ServiceDS.shared.address = nil
            UIView.animate(withDuration: 0.3) {
                self.placeListHeight.constant = 0
                self.view.layoutIfNeeded()
                self.placeList = []
            }
            locationManager.startUpdatingLocation()
        }else{
            placeName.text = ""
            addressLabel.text = searchText!
            ServiceDS.shared.address = searchText!
            ServiceDS.shared.location = (nil, nil)
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
      
    }
}

