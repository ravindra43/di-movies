//
//  BaseViewController.swift
//  DavisImperial
//
//  Created by Surendra on 12/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class BaseViewController: UIViewController, SlideMenuControllerDelegate {

    var overlay : UIView!;
    var addSideMenuButton = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.overlay = UIView.init(frame: self.view.frame);
        self.view.addSubview(self.overlay);
        self.overlay.backgroundColor = Color.blackOverlay();
        self.showOverlay(false);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.addSideMenuButton && self.navigationController?.viewControllers.count == 1 {
            self.slideMenuController()?.delegate = self;
            
            let item = UIBarButtonItem.init(image: UIImage.init(named: "topbarMenu"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didTapTopMenu))
            self.navigationItem.leftBarButtonItem = item;
        }
    }
    
    func tapOnOverlay() {
        guard let slideMenu = self.slideMenuController() else {
            return;
        }
        
        if (slideMenu.isLeftOpen()) {
            slideMenu.closeLeft();
        }
    }
    
    func showOverlay(_ show : Bool) {
        self.overlay.alpha = show ? 1.0 : 0.0;
    }
    
    @objc func didTapTopMenu() {
        guard let slideMenu = self.slideMenuController() else {
            return;
        }
        
        if (slideMenu.isLeftOpen()) {
            slideMenu.closeLeft();
        } else {
            slideMenu.openLeft();
        }
    }
    
    func leftWillOpen() {
        self.showOverlay(true);
    }
    
    func leftWillClose() {
        self.showOverlay(false);
    }
}
