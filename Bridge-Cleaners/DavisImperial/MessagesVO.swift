//
//  MessagesVO.swift
//  DavisImperial
//
//  Created by Ravindra Singh on 09/01/18.
//  Copyright © 2018 Encoresky. All rights reserved.
//

import UIKit

class MessagesVO: NSObject {
    var from : String!
    var message : String?
    var createdDate : Date?
    var isSender: Bool?
    init(from: String) {
        super.init();
        self.from = from
        self.isSender = from == UserManager.shared.currentUser?.userId
    }
}

extension MessagesVO {
    convenience init?(json: [String: Any]) {
        
        guard let _from = json["sender"] as? String
            else {
                return nil
        }
        
        self.init(from: _from);
        
        if let _msg = json["message"] as? String {
            self.message = _msg
        }
        
        if let _date = json["created_at"] as? String {
            self.createdDate = Globals.dateFromString(string: _date)
        }
    }
    
}
