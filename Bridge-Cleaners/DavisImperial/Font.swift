//
//  Font.swift
//  DavisImperial
//
//  Created by Surendra on 8/6/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit
/*

 
 font heading= Adobe Caslon Pro  - bold  - 42
 
 font paragraph= Avenir Next LT Pro  - regular - 20
 
 
 login/signup-
 
 buttun font-  Roboto - regular  - 24
 
 placeholder font - Roboto - light - 20
 
 
 Note: instead of (roboto) we can use (Avenir Next LT Pro).
 
*/
class Font: NSObject {

    static func regularFontOf(size:CGFloat) -> UIFont? {
        return UIFont.init(name: "AvenirNextLTPro-Regular", size: size);
    }
    
    static func lightFontOf(size:CGFloat) -> UIFont? {
        return UIFont.init(name: "AvenirNextLTPro-Light", size: size);
    }
    
    static func headingFontOf(size:CGFloat) -> UIFont? {
        return UIFont.init(name: "ACaslonPro-Bold", size: size);
    }

}
