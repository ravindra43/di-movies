//
//  Strings.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 08/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import Foundation

var diEnterFirstNameMSG = "Please enter First name"
var diEnterLastNameMSG  = "Please enter Last name"
var diEnterProdutionMSG = "Please enter Production name"
var diEnterPositionMSG  = "Please enter your Position"
var diEnterEmailMSG     = "Please enter a valid email address"
var diEnterMobileMSG    = "Please enter a valid mobile number"
var diEnterPasswordMSG  = "Please enter password"
var diPasswordLentghMSG = "Password should have minimum 3 digits"
