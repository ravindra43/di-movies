//
//  UFButton.swift
//  DavisImperial
//
//  Created by Surendra on 6/28/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class UFButton: UIButton {

    @IBInspectable var isSecondaryStyle : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateUI()
    }
    
    func updateUI() {
        self.layer.cornerRadius = self.bounds.size.height/2.0
        
//        if !self.isSecondaryStyle {
//            self.backgroundColor = Color.gray()
//            self.setTitleColor(.white, for: .normal)
//
//        } else {
//            self.backgroundColor = UIColor.init(white: 0.9, alpha: 1.0)
//            self.setTitleColor(UIColor.darkGray, for: .normal)
//        }
//        self.titleLabel?.font = Font.regularFontOf(size: 15.0)
        
//        self.backgroundColor = UIColor(named: Color.buttonBackgroundColor)
        self.setTitleColor(UIColor(named: Color.buttonTextColor), for: .normal)
        self.titleLabel?.font = Font.regularFontOf(size: 15.0)
    }

}
