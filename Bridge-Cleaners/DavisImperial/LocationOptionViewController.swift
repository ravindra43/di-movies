//
//  LocationOptionViewController.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 18/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import UIKit

class LocationOptionViewController: UIViewController {
    
    let vc = BagSelectionViewController.getInstance()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pick Up Location"
        self.navigationItem.backBarButtonItem = self.emptyBackBarButton()
    }
    
    static func getInstance() -> LocationOptionViewController {
        return Globals.mainStoryboard().instantiateViewController(withIdentifier: kLTOptionVCID) as! LocationOptionViewController
    }
    
    @IBAction func garmentStorage(_ sender: UIButton) {
        ServiceDS.shared.address = sender.titleLabel?.text
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPressWardrobe(_ sender: UIButton) {
        ServiceDS.shared.address = sender.titleLabel?.text
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPressCostumeOffice(_ sender: UIButton) {
        ServiceDS.shared.address = sender.titleLabel?.text
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPressOnlocation(_ sender: UIButton) {
        if #available(iOS 9.3, *) {
            let vc = LocationViewController.getInstance()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
