//
//  DataSourceStatus.swift
//  DavisImperial
//
//  Created by Surendra on 6/30/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

enum DataSourceStatus {
    case idle
    case loading
    case failed
}

class BaseDataSource : NSObject {
    
    var allItemsLoaded = false;
    var status = DataSourceStatus.idle;
    var isReloading = false
    var success: (() -> Void)?;
    var failure: (() -> Void)?;
    
    var pageSize : UInt16 = 20
    var page : UInt16 = 1
    
    override init() {}
    
    convenience init(success: @escaping (() -> Void), failure: @escaping (() -> Void)) {
        self.init();
        self.success = success;
        self.failure = failure;
    }
    
    func loadNextPage() {
        if !self.allItemsLoaded {
            self.page += 1
            self.loadItems()
        }
    }
    
    func loadItems() {}
    
    func cancelLoading()  {
        self.status = .idle
    }
    
    func reloadData() {
        self.isReloading    = true
        self.allItemsLoaded = false
        self.page           = 1
        self.status         = .idle
        self.loadItems();
    }
}
