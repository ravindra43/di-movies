//
//  SocketManager.swift
//  Chat Gravity
//
//  Created by Rahul Kumawat on 25/01/18.
//  Copyright © 2018 Encoresky. All rights reserved.
//

import UIKit
import SocketIO

let maneger = SocketManager(socketURL: URL(string: APIRequest.apiBaseURL())!, config: [.connectParams(["userId" : (UserManager.shared.currentUser?.userId)!])])
var socket = maneger.defaultSocket
let kResendMessages = "ResendMessage"
var onlineUsers = [String]()
var isConnected = false
class SocketIOManager: NSObject {
  
    static let shared = SocketIOManager()
    var delegate: Socketdelegate?
    
    enum type: String {
        case image    = "image"
        case video    = "video"
    }
    
    enum updateType: String {
        case add = "add"
        case remove = "remove"
    }
    
    override init() {
        super.init()
        self.addHandlers()
        socket.on(clientEvent: .connect) {data, ack in
            print(data,"Socket Connected")
            isConnected = true
            UserDefaults.standard.removeObject(forKey: kResendMessages)
        }
        socket.on(clientEvent: .disconnect) { (data, ack) in
            isConnected = false
            print("Socket Disconnected")
        }
        
        socket.on("new_message") { (data, ack) in
            print("\n=== CHAT RECEIVED ==\n",data)
            let chatObj = data[0] as! [String:Any]
            self.delegate?.didReceivedMessage!(messageObj: chatObj)
        }
    }
 
    func addHandlers() {
        if UserManager.hasValidSession() {
            socket.connect()
        }
    }

    func establishConnection() {
        if UserManager.hasValidSession() {
            socket.connect()
        }
    }
    
    func closeConnection() {
        if UserManager.hasValidSession() {
            socket.disconnect()
        }
    }
}
@objc protocol Socketdelegate {
    @objc optional func didReceivedMessage(messageObj:[String: Any])
}
