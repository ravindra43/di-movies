//
//  KeychainManager.swift
//  DavisImperial
//
//  Created by Rahul Kumawat on 08/04/19.
//  Copyright © 2019 Encoresky. All rights reserved.
//

import Foundation
import LocalAuthentication

class KeychainManager {
    static var shared: KeychainManager = KeychainManager()
    var result: ((_ account: String, _ password: String) -> Void)?;
    
    func saveAccountDetailsToKeychain(account: String, password: String) {
        guard !account.isEmpty, !password.isEmpty else { return }
        UserDefaults.standard.set(account, forKey: diAccountKey)
        print("DATA", account, password)
        let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: account, accessGroup: KeychainConfiguration.accessGroup)
        do {
            try passwordItem.savePassword(password)
            print("saved")
        } catch {
            print("Error saving password")
        }
    }
    
    func evaulateTocuhIdAuthenticity(context: LAContext, result: @escaping (_ account: String, _ password: String) -> Void) {
        if #available(iOS 11.0, *) {
            if (context.biometryType == LABiometryType.faceID) {
                print("FaceId support")
            } else if (context.biometryType == LABiometryType.touchID) {
                print("TouchId support")
            } else {
                print("No Biometric support")
            }
        }
        guard let lastAccessedUserName = UserDefaults.standard.object(forKey: diAccountKey) as? String else { return }
        var laPolicy:LAPolicy = .deviceOwnerAuthentication
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            laPolicy = .deviceOwnerAuthenticationWithBiometrics
        }
        context.evaluatePolicy(laPolicy, localizedReason: lastAccessedUserName) { (authSuccessful, authError) in
            if authSuccessful {
                self.loadPasswordFromKeychainAndAuthenticateUser(lastAccessedUserName)
            } else {
                DispatchQueue.main.async {
                    if let error = authError as? LAError {
                        self.showError(error: error)
                    }
                }
            }
        }
        self.result = result
    }
    
    func loadPasswordFromKeychainAndAuthenticateUser(_ account: String) {
        guard !account.isEmpty else { return }
        let passwordItem = KeychainPasswordItem(service:   KeychainConfiguration.serviceName, account: account, accessGroup: KeychainConfiguration.accessGroup)
        do {
            let storedPassword = try passwordItem.readPassword()
            self.result!(account, storedPassword)
        } catch KeychainPasswordItem.KeychainError.noPassword {
            print("No saved password")
        } catch {
            print("Unhandled error")
        }
    }
    
    func showError(error: LAError) {
        var message: String = ""
        switch error.code {
        case LAError.authenticationFailed:
            message = "Authentication was not successful because the user failed to provide valid credentials. Please enter password to login."
            break
        case LAError.userFallback:
            message = "Authentication was canceled because the user tapped the fallback button"
            break
        case LAError.biometryNotEnrolled:
            message = "Authentication could not start because Touch ID has no enrolled fingers."
            break
        case LAError.passcodeNotSet:
            message = "Passcode is not set on the device."
            break
        case LAError.systemCancel:
            message = "Authentication was canceled by system"
            break
        default:
            return
        }
        
        Globals.showGenericAlert(title: "", message: message)
    }
}
