//
//  LeftMenuCell.swift
//  DavisImperial
//
//  Created by Surendra on 12/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {

    @IBOutlet weak var menuTitle : UILabel!;
    
    override func awakeFromNib() {
        self.selectionStyle = UITableViewCell.SelectionStyle.default;
        let bgColorView = UIView.init();
        bgColorView.backgroundColor = UIColor.lightGray;
        self.selectedBackgroundView = bgColorView;
        self.menuTitle.font = UIFont.systemFont(ofSize: 15.0)
    }
    
}
