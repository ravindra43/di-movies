//
//  APIRequest.swift
//  DavisImperial
//
//  Created by Surendra on 16/05/17.
//  Copyright © 2017 Encoresky. All rights reserved.
//

import Foundation

class APIRequest : NSObject, NetworkServiceDelegate {
    
    var service : NetworkService?;
    var result :  ((_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void)?;
    var isRunning = true
    static var requestPool = [APIRequest]()
    private init(url : String) {
        super.init();
        self.service = NetworkService.init(WithURL: url, delegate: self);
        self.service?.enableLogging = !Globals.isProd()
        if UserManager.hasValidSession() {
            self.service?.headers?["Authorization"] = "Bearer " + UserManager.shared.sessionToken()!;
        }
        self.service?.headers?["Content-Type"] = "application/json"
        APIRequest.requestPool.append(self)
    }
    
    static func apiBaseURL() -> String {
        return Globals.isProd() ? "https://codhod.com:9102/" : "http://192.168.1.12:4321/";
    }
    
    func cancel() {
        self.service?.cancel();
        self.cleanup();
        self.isRunning = false
    }
    
    func cleanup() {
        self.service = nil;
        self.service?.delegate = nil;
        self.result = nil;
        self.isRunning = false
    }
    
    static func isNetworkAvailable() -> Bool{
        return NetworkService.isNetworkAvailable();
    }
    
    static func checkNetworkConnectionWithCompletion(completion : @escaping (_ isAvailable : Bool) -> Void) {
        NetworkService.checkNetworkConnectionWith(completion: completion);
    }
    
    //MARK :- NetworkServiceDelegate
    func networkService(service: NetworkService, response: Any?, error: Error?) {
        self.isRunning = false
        self.result?(self,response,error);
        if APIRequest.requestPool.firstIndex(of: self) != nil {
            APIRequest.requestPool.remove(at: APIRequest.requestPool.firstIndex(of: self)!)
        }
        
        if let responseObj = response as? [String : Any] {
            let status = Globals.getUnsignedNum(object: responseObj["status"])
            if status == 401 {
                UserManager.logout()
            }
        }
    }
    
    static func login(email : String , password : String, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "user/login";
        
        let request = APIRequest.init(url: url);
        request.service?.params["email"] = email;
        request.service?.params["password"] = password;
        request.service?.params["device_type"] = "ios";
        request.service?.params["device_token"] = PushNotificationManager.shared.apnsToken;
        request.result = result
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest()
        
        return request;
    }
    
    
    static func changePassword(oldPassword : String , newpPassword : String, cNewPassword : String, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "user/change-password";
        
        let request = APIRequest.init(url: url);
        request.service?.params["old_password"] = oldPassword;
        request.service?.params["new_password"] = newpPassword;
        
        request.result = result
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPUTRequest()
        
        return request;
    }
    
    static func resetPassword(email: String, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "user/forgot-password";
        
        let request = APIRequest.init(url: url);
        request.service?.params["email"] = email;
        
        request.result = result
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPUTRequest()
        
        return request;
    }
    
    static func removeDeviceToken(apnsToken : String, result : ((_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void)? = nil) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "users/removeDeviceToken";
        
        let request = APIRequest.init(url: url);
        request.service?.params["device_token"] = apnsToken;
        request.service?.params["device_type"] = "iOS"
        request.service?.params["user_id"] = UserManager.shared.currentUser?.userId
        
        request.result = result
        request.service?.responseType = APIResponseType.JSON
        request.service?.sendPOSTRequest()
        
        return request;
    }
    
    static func bookServise( result : ((_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void)? = nil) -> APIRequest {
        let (lat, long) = ServiceDS.shared.location
        let url = APIRequest.apiBaseURL() + "booking/add";
        let request = APIRequest.init(url: url);
        request.service?.params["instruction"]  = ServiceDS.shared.instructions;
        request.service?.params["propose_date"] = ServiceDS.shared.scheduleDate
        request.service?.params["preferred_delivery_date"] = ServiceDS.shared.deliveryDate
        if lat != nil && long != nil{
            request.service?.params["lat_long"]      = [lat!, long!]
        }
        request.service?.params["address"]      = ServiceDS.shared.address;
        request.service?.params["total_bags"]   = ServiceDS.shared.numberOfBags;
        request.service?.params["barcodes"]     = ServiceDS.shared.barCodes
        request.service?.params["no_rush"]     = ServiceDS.shared.noRush
        
        request.result = result
        request.service?.responseType = APIResponseType.JSON
        request.service?.sendPOSTRequest()
        
        return request;
    }
    
    static func registerAPNSToken(apnsToken : String?, result : ((_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void)? = nil) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "users/saveDeviceToken";
        
        let request = APIRequest.init(url: url);
        if apnsToken != nil {
            request.service?.params["device_token"] = apnsToken
        }
        request.service?.params["device_type"] = "iOS"
        request.service?.params["user_id"] = UserManager.shared.currentUser?.userId
        
        request.result = result;
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest();
        
        return request;
    }
    
    static func register(firstName: String, lastName: String, email : String ,phone : String,password : String, production : String, position: String, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "user/registration";
        
        let request = APIRequest.init(url: url);
        request.service?.params["first_name"] = firstName;
        request.service?.params["last_name"] = lastName;
        request.service?.params["email"] = email;
        request.service?.params["phone"] = phone;
        request.service?.params["password"] = password;
        request.service?.params["production_name"] = production;
        request.service?.params["position"] = position;
        
        request.result = result;
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest();
        
        return request;
    }
    
    static func sendOtp(phone : String, result : ((_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void)? = nil) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "send-otp";
        
        let request = APIRequest.init(url: url);
        request.service?.params["email"] = phone
        request.result = result;
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest();
        
        return request;
    }
    
    static func verifyOtp(phone : String, otp : String, result : ((_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void)? = nil) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "verify-otp";
        
        let request = APIRequest.init(url: url);
        request.service?.params["email"] = phone
        request.service?.params["verification_code"] = otp
        request.result = result;
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest();
        
        return request;
    }
    
    static func getSessionUser(result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "users/profile/" +  (UserManager.shared.currentUser?.userId)!;
        
        let request = APIRequest.init(url: url);
        request.service?.headers?["Content-Type"] = ""
        request.result = result;
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendGETRequest();
        
        return request;
    }
    
    static func book(date:Date, comments:String, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.FFFZ"
        _ = dateFormatter.string(from: date)
        
        let url = APIRequest.apiBaseURL() + "bookings/book";
        let request = APIRequest.init(url: url);
        request.service?.sendPOSTRequest();
        
        return request;
    }
    
    static func getBookings(pageNo: Int, limit: Int, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "booking/my-bookings/\(pageNo)/\(limit)"
        let request = APIRequest.init(url: url);
        request.result = result;
        request.service?.headers?["Content-Type"] = ""
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendGETRequest();
        
        return request;
    }
    
    static func getMessages(pageNo: Int, limit: Int, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "chat/messages/\(pageNo)/\(limit)"
        let request = APIRequest.init(url: url);
        request.result = result;
        request.service?.headers?["Content-Type"] = ""
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendGETRequest();
        
        return request;
    }
    
    static func getNotifications(forActivity: Bool, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        
        let url = forActivity ? APIRequest.apiBaseURL() + "users/getActivityNotifications" : APIRequest.apiBaseURL() + "bookings/getNotifications/" + (UserManager.shared.currentUser?.userId)!;
        let request = APIRequest.init(url: url);
        request.result = result;
        request.service?.headers?["Content-Type"] = ""
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendGETRequest();
        
        return request;
    }
    
    static func getUnreadCount(result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        
        let url = APIRequest.apiBaseURL() + "users/get-unread-messages-count";
        let request = APIRequest.init(url: url);
        request.result = result;
        request.service?.headers?["Content-Type"] = ""
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendGETRequest();
        
        return request;
    }
    
    
    static func sendMessage(message : String, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "chat/to-admin";
        
        let request = APIRequest.init(url: url);
        request.service?.params["message"] = message;
        request.result = result
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest()
        
        return request;
    }
    
    static func respondToBooking(bookingId:String, accept:Bool, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "booking/cancel-my-booking/\(bookingId)";
        let request = APIRequest.init(url: url);
        request.result = result;
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest();
        
        return request;
    }
    
    static func updateUserProfile(name: String, email : String ,phone : String, address : String, city: String, result : @escaping (_ service: APIRequest?, _ response: Any?, _ error: Error?) -> Void) -> APIRequest {
        let url = APIRequest.apiBaseURL() + "users/updateProfile";
        
        let request = APIRequest.init(url: url);
        request.service?.params["user_id"] = UserManager.shared.currentUser?.userId;
        request.service?.params["name"] = name;
        request.service?.params["email"] = email;
        request.service?.params["phone"] = phone;
        request.service?.params["address"] = address;
        request.service?.params["city"] = city;
        
        request.result = result;
        request.service?.responseType = APIResponseType.JSON;
        request.service?.sendPOSTRequest();
        
        return request;
    }
}
